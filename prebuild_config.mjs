// @ts-check
import { readFileSync, writeFileSync } from 'fs'
import path from 'path'
import { csvParse } from 'd3-dsv'
import { group } from 'd3-array'
import { capitalize, separateWords } from './src/helpers.js'

/**
 * La intension de este archivo es tener un punto de partida para hacer los
 * archivos de configuraciones para poder alimentar los generadores de archivos.
 */


let __dirname = path.resolve(path.dirname(''))

/**
 * Leemos los datos que vienen de la base de datos, estoy los saco con el query
 * que viene en el siguiente archivo
 * docs/dynamic database/get all fields.md
 */
const file_data = readFileSync(path.join(__dirname, 'reloj_fields.csv'), 'utf8')

let data = csvParse(file_data)

let tables = [...group(data, d => d.table_name).entries()]

let fileContents = tables.map(([key, value], i) => {
    let fields = value.map(r => `new Field({name:'${r.COLUMN_NAME}', type: '${r.COLUMN_TYPE}', comment:'${r.COLUMN_COMMENT}', length:${r.CHARACTER_MAXIMUM_LENGTH || 0}, columnType:'${r.DATA_TYPE}', nullable:${r.IS_NULLABLE === 'YES' ? true : false}, label:'${capitalize(separateWords(r.COLUMN_NAME))}', key:${r.COLUMN_KEY==='PRI'?true:false} }),`).join('\n        ')
    let varname = key.replace(/-/g, '')
    return `let ${varname} = new Table({
    name: '${key}',
    table: '${key}',
    connection: 'mysql',
    fields: [
        ${fields}
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    `
});

let keys = tables.map(([k]) => k.replace(/-/g, ''))
fileContents.unshift(`// @ts-check\nimport {Table, Field} from './src/BaseObjects.js'\n`)
fileContents.push(`/** @type {Table[]} */\nlet allTables = [\n${keys.join(',\n')}\n]\nexport {allTables}`)

// console.log(keys)
// console.log(fileContents)

writeFileSync(path.join(__dirname, 'databaseConfiguration.mjs'), fileContents.join('\n'))