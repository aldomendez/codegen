// @ts-check
import { allTables } from './databaseConfiguration.js'
import laravelModels from './plugins/laravelModels.js'
import laravelController from './plugins/laravelController.js'
import laravelRoute from './plugins/laravelRoute.js'
import vueRouterGenerator from './plugins/vueRouterGenerator.js'
import svelteModelGenerator from './plugins/svelte_model.js'
import vueCrud from './plugins/vueCrud.js'

let targetForlder = '/svelte'

/**
 * This part of the creator process is focused on the BOM material
 * 
 * This is the main focus right now because I need to present it ASAP
 * and my time as always is limited
 */
let BOMRelatedTables = allTables
    // .filter((_, i) => i < 5)
    // .filter( p => false)
    .filter((p) => [
        'bom_lists',
        'bill_of_materials',
        'routings',
        'route_definitions',
        'parts'
    ].includes(p.name))


// let SvelteThings = allTables
//     .filter((p) => [
//         'signages',
//         'signage_slides'
//     ].includes(p.name))

// svelteModelGenerator('svelteGenerated/')(SvelteThings)

BOMRelatedTables
    .map(laravelModels)
    .map(laravelController)
    .map(vueCrud('frontend/bill_of_materials/'))

// laravelRoute(
//     BOMRelatedTables
// )

// vueRouterGenerator('frontend/bill_of_materials/src/router/', 'cruds')(
//     BOMRelatedTables
// )
