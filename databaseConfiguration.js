// @ts-check
import {Table, Field} from './src/BaseObjects.js'

let absent_people = new Table({
    name: 'absent_people',
    table: 'absent_people',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'varchar(5)', comment:'', length:5, columnType:'varchar', nullable:true, label:'Id', key:false }),
        new Field({name:'name', type: 'char(50)', comment:'', length:50, columnType:'char', nullable:false, label:'Name', key:false }),
        new Field({name:'gender', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Gender', key:false }),
        new Field({name:'shift', type: 'char(2)', comment:'', length:2, columnType:'char', nullable:false, label:'Shift', key:false }),
        new Field({name:'bar_code', type: 'decimal(8,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Bar code', key:false }),
        new Field({name:'company_prefix', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:true, label:'Company prefix', key:false }),
        new Field({name:'company', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Company', key:false }),
        new Field({name:'department', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Department', key:false }),
        new Field({name:'position', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Position', key:false }),
        new Field({name:'supervisor', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Supervisor', key:false }),
        new Field({name:'organization', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Organization', key:false }),
        new Field({name:'birth_date', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'Birth date', key:false }),
        new Field({name:'hire_date', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'Hire date', key:false }),
        new Field({name:'is_active', type: 'tinyint(4)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is active', key:false }),
        new Field({name:'is_work_union', type: 'tinyint(4)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is work union', key:false }),
        new Field({name:'is_absent', type: 'bigint(20)', comment:'', length:0, columnType:'bigint', nullable:false, label:'Is absent', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let account = new Table({
    name: 'account',
    table: 'account',
    connection: 'mysql',
    fields: [
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:true }),
        new Field({name:'username', type: 'varchar(255)', comment:'', length:255, columnType:'varchar', nullable:false, label:'Username', key:false }),
        new Field({name:'password', type: 'varchar(60)', comment:'', length:60, columnType:'varchar', nullable:false, label:'Password', key:false }),
        new Field({name:'is_admin', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is admin', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let activities = new Table({
    name: 'activities',
    table: 'activities',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Type', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'User id', key:false }),
        new Field({name:'answer', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Answer', key:false }),
        new Field({name:'round', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Round', key:false }),
        new Field({name:'work_order_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Work order id', key:false }),
        new Field({name:'comments', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Comments', key:false }),
        new Field({name:'token', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Token', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let areas = new Table({
    name: 'areas',
    table: 'areas',
    connection: 'mysql',
    fields: [
        new Field({name:'code', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Code', key:false }),
        new Field({name:'id', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'cm101', type: 'char(5)', comment:'', length:5, columnType:'char', nullable:false, label:'Cm101', key:false }),
        new Field({name:'cm102', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'Cm102', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let assid = new Table({
    name: 'assid',
    table: 'assid',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'char(8)', comment:'', length:8, columnType:'char', nullable:false, label:'Id', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let breaks_schedule = new Table({
    name: 'breaks_schedule',
    table: 'breaks_schedule',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'shift', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Shift', key:false }),
        new Field({name:'start', type: 'time', comment:'', length:0, columnType:'time', nullable:false, label:'Start', key:false }),
        new Field({name:'end', type: 'time', comment:'', length:0, columnType:'time', nullable:false, label:'End', key:false }),
        new Field({name:'duration_in_minutes', type: 'time', comment:'', length:0, columnType:'time', nullable:false, label:'Duration in minutes', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let break_employee = new Table({
    name: 'break_employee',
    table: 'break_employee',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'employee_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Employee id', key:false }),
        new Field({name:'break_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Break id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let broker_agencies = new Table({
    name: 'broker_agencies',
    table: 'broker_agencies',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let carriers = new Table({
    name: 'carriers',
    table: 'carriers',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'address', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Address', key:false }),
        new Field({name:'phone', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Phone', key:false }),
        new Field({name:'contact_person_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Contact person name', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let category = new Table({
    name: 'category',
    table: 'category',
    connection: 'mysql',
    fields: [
        new Field({name:'code', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Code', key:false }),
        new Field({name:'id', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'cm101', type: 'char(5)', comment:'', length:5, columnType:'char', nullable:false, label:'Cm101', key:false }),
        new Field({name:'cm102', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'Cm102', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let change_records = new Table({
    name: 'change_records',
    table: 'change_records',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'model', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Model', key:false }),
        new Field({name:'record_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Record id', key:false }),
        new Field({name:'comments', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Comments', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let clients = new Table({
    name: 'clients',
    table: 'clients',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'prefix', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Prefix', key:false }),
        new Field({name:'is_active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is active', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let clock_movements = new Table({
    name: 'clock_movements',
    table: 'clock_movements',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'varchar(5)', comment:'', length:5, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'cid', type: 'char(8)', comment:'', length:8, columnType:'char', nullable:false, label:'Cid', key:false }),
        new Field({name:'station', type: 'char(2)', comment:'', length:2, columnType:'char', nullable:false, label:'Station', key:false }),
        new Field({name:'original_date', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:false, label:'Original date', key:false }),
        new Field({name:'adjusted_date', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:false, label:'Adjusted date', key:false }),
        new Field({name:'movement', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Movement', key:false }),
        new Field({name:'was_inserted', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Was inserted', key:false }),
        new Field({name:'was_deleted', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Was deleted', key:false }),
        new Field({name:'was_not_adjusted', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Was not adjusted', key:false }),
        new Field({name:'day_of_week', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Day of week', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let companies = new Table({
    name: 'companies',
    table: 'companies',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'prefix', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Prefix', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'parasql_id', type: 'bigint(20)', comment:'', length:0, columnType:'bigint', nullable:true, label:'Parasql id', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let companies_reloj = new Table({
    name: 'companies_reloj',
    table: 'companies_reloj',
    connection: 'mysql',
    fields: [
        new Field({name:'code', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Code', key:false }),
        new Field({name:'id', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'cm101', type: 'char(5)', comment:'', length:5, columnType:'char', nullable:false, label:'Cm101', key:false }),
        new Field({name:'cm102', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'Cm102', key:false }),
        new Field({name:'order', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Order', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let company_invoices = new Table({
    name: 'company_invoices',
    table: 'company_invoices',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'shipment_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Shipment id', key:false }),
        new Field({name:'invoice_type_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Invoice type id', key:false }),
        new Field({name:'company_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Company id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let company_user = new Table({
    name: 'company_user',
    table: 'company_user',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'company_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Company id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let courses = new Table({
    name: 'courses',
    table: 'courses',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'code', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Code', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let courses_takens = new Table({
    name: 'courses_takens',
    table: 'courses_takens',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'course_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Course id', key:false }),
        new Field({name:'first_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'First name', key:false }),
        new Field({name:'last_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Last name', key:false }),
        new Field({name:'email', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Email', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let cumple_file_meta = new Table({
    name: 'cumple_file_meta',
    table: 'cumple_file_meta',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'file_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'File id', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'system_identifier', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'System identifier', key:false }),
        new Field({name:'author', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Author', key:false }),
        new Field({name:'last_author', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Last author', key:false }),
        new Field({name:'file_created_at', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'File created at', key:false }),
        new Field({name:'file_modified_at', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'File modified at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let departments = new Table({
    name: 'departments',
    table: 'departments',
    connection: 'mysql',
    fields: [
        new Field({name:'code', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Code', key:false }),
        new Field({name:'id', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'cm101', type: 'char(5)', comment:'', length:5, columnType:'char', nullable:false, label:'Cm101', key:false }),
        new Field({name:'cm102', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'Cm102', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let digital_signages = new Table({
    name: 'digital_signages',
    table: 'digital_signages',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Location', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Name', key:false }),
        new Field({name:'ip', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Ip', key:false }),
        new Field({name:'mac', type: 'varchar(17)', comment:'', length:17, columnType:'varchar', nullable:true, label:'Mac', key:false }),
        new Field({name:'model', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Model', key:false }),
        new Field({name:'url', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Url', key:false }),
        new Field({name:'owner', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Owner', key:false }),
        new Field({name:'description', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Description', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let divisions = new Table({
    name: 'divisions',
    table: 'divisions',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'company_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Company id', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Name', key:false }),
        new Field({name:'code', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Code', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Description', key:false }),
        new Field({name:'address', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Address', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let drivers = new Table({
    name: 'drivers',
    table: 'drivers',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'carrier_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Carrier id', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let d_b_f_directories = new Table({
    name: 'd_b_f_directories',
    table: 'd_b_f_directories',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'input', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Input', key:false }),
        new Field({name:'output', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Output', key:false }),
        new Field({name:'active', type: 'tinyint(4)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let d_b_f_files = new Table({
    name: 'd_b_f_files',
    table: 'd_b_f_files',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'directory_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Directory id', key:false }),
        new Field({name:'file_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'File name', key:false }),
        new Field({name:'base_table_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Base table name', key:false }),
        new Field({name:'table_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Table name', key:false }),
        new Field({name:'mask', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Mask', key:false }),
        new Field({name:'just', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:true, label:'Just', key:false }),
        new Field({name:'upload', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Upload', key:false }),
        new Field({name:'convert', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Convert', key:false }),
        new Field({name:'canUpdate', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'CanUpdate', key:false }),
        new Field({name:'update_mask', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Update mask', key:false }),
        new Field({name:'table_creation', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Table creation', key:false }),
        new Field({name:'post_creation_queries', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Post creation queries', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let d_files = new Table({
    name: 'd_files',
    table: 'd_files',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'directory_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Directory id', key:false }),
        new Field({name:'file_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'File name', key:false }),
        new Field({name:'base_table_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Base table name', key:false }),
        new Field({name:'table_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Table name', key:false }),
        new Field({name:'mask', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Mask', key:false }),
        new Field({name:'upload', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Upload', key:false }),
        new Field({name:'convert', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Convert', key:false }),
        new Field({name:'canUpdate', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'CanUpdate', key:false }),
        new Field({name:'update_mask', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Update mask', key:false }),
        new Field({name:'table_creation', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Table creation', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let d_folders = new Table({
    name: 'd_folders',
    table: 'd_folders',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'input', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Input', key:false }),
        new Field({name:'output', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Output', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let employees = new Table({
    name: 'employees',
    table: 'employees',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'client_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Client id', key:false }),
        new Field({name:'position', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Position', key:false }),
        new Field({name:'line', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Line', key:false }),
        new Field({name:'break_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Break id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let employees_not_checking_entrance = new Table({
    name: 'employees_not_checking_entrance',
    table: 'employees_not_checking_entrance',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'employee_id', type: 'varchar(6)', comment:'', length:6, columnType:'varchar', nullable:false, label:'Employee id', key:false }),
        new Field({name:'creation_date', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:false, label:'Creation date', key:false }),
        new Field({name:'update_dt', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:false, label:'Update dt', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let employee_shift = new Table({
    name: 'employee_shift',
    table: 'employee_shift',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'employee_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Employee id', key:false }),
        new Field({name:'shift_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Shift id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let event = new Table({
    name: 'event',
    table: 'event',
    connection: 'mysql',
    fields: [
        new Field({name:'event_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Event id', key:true }),
        new Field({name:'website_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Website id', key:false }),
        new Field({name:'session_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Session id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Created at', key:false }),
        new Field({name:'url', type: 'varchar(500)', comment:'', length:500, columnType:'varchar', nullable:false, label:'Url', key:false }),
        new Field({name:'event_type', type: 'varchar(50)', comment:'', length:50, columnType:'varchar', nullable:false, label:'Event type', key:false }),
        new Field({name:'event_value', type: 'varchar(50)', comment:'', length:50, columnType:'varchar', nullable:false, label:'Event value', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let failed_jobs = new Table({
    name: 'failed_jobs',
    table: 'failed_jobs',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'uuid', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Uuid', key:false }),
        new Field({name:'connection', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Connection', key:false }),
        new Field({name:'queue', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Queue', key:false }),
        new Field({name:'failed_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Failed at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let failure_modes = new Table({
    name: 'failure_modes',
    table: 'failure_modes',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let fields = new Table({
    name: 'fields',
    table: 'fields',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'title', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Title', key:false }),
        new Field({name:'table_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Table name', key:false }),
        new Field({name:'type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Type', key:false }),
        new Field({name:'column', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Column', key:false }),
        new Field({name:'cols', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Cols', key:false }),
        new Field({name:'label', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Label', key:false }),
        new Field({name:'field', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Field', key:false }),
        new Field({name:'order', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Order', key:false }),
        new Field({name:'help', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Help', key:false }),
        new Field({name:'notes', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Notes', key:false }),
        new Field({name:'max_length', type: 'int(10)', comment:'', length:0, columnType:'int', nullable:true, label:'Max length', key:false }),
        new Field({name:'is_required', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is required', key:false }),
        new Field({name:'validation', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Validation', key:false }),
        new Field({name:'is_primary', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Is primary', key:false }),
        new Field({name:'is_heading', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is heading', key:false }),
        new Field({name:'heading_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Heading id', key:false }),
        new Field({name:'schema', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Schema', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let files = new Table({
    name: 'files',
    table: 'files',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'file_path', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'File path', key:false }),
        new Field({name:'original_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Original name', key:false }),
        new Field({name:'type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Type', key:false }),
        new Field({name:'extension', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Extension', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let freight_boxes = new Table({
    name: 'freight_boxes',
    table: 'freight_boxes',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'carrier_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Carrier id', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let groups = new Table({
    name: 'groups',
    table: 'groups',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'is_personal', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Is personal', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let group_user = new Table({
    name: 'group_user',
    table: 'group_user',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'group_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Group id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let hartland_parts = new Table({
    name: 'hartland_parts',
    table: 'hartland_parts',
    connection: 'mysql',
    fields: [
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:true }),
        new Field({name:'part_in_catalog', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part in catalog', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'unit_of_measure', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Unit of measure', key:false }),
        new Field({name:'part_type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part type', key:false }),
        new Field({name:'manufacturer', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Manufacturer', key:false }),
        new Field({name:'is_internal', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is internal', key:false }),
        new Field({name:'created_by', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Created by', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let hartland_part_transactions = new Table({
    name: 'hartland_part_transactions',
    table: 'hartland_part_transactions',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:false }),
        new Field({name:'in', type: 'decimal(15,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'In', key:false }),
        new Field({name:'out', type: 'decimal(15,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Out', key:false }),
        new Field({name:'location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Location', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let health_check_pings = new Table({
    name: 'health_check_pings',
    table: 'health_check_pings',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'health_check_services_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Health check services id', key:false }),
        new Field({name:'service_code', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Service code', key:false }),
        new Field({name:'ip', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Ip', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let health_check_services = new Table({
    name: 'health_check_services',
    table: 'health_check_services',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Description', key:false }),
        new Field({name:'schedule', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Schedule', key:false }),
        new Field({name:'grace', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Grace', key:false }),
        new Field({name:'service_code', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Service code', key:false }),
        new Field({name:'active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'timezone', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Timezone', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let informacion_personals = new Table({
    name: 'informacion_personals',
    table: 'informacion_personals',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'direccion', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Direccion', key:false }),
        new Field({name:'codigo_postal', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Codigo postal', key:false }),
        new Field({name:'colonia', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Colonia', key:false }),
        new Field({name:'telefono1', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Telefono1', key:false }),
        new Field({name:'telefono2', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Telefono2', key:false }),
        new Field({name:'responsable_emergencia', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Responsable emergencia', key:false }),
        new Field({name:'telefono_emergencia', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Telefono emergencia', key:false }),
        new Field({name:'responsable_emergencia_2', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Responsable emergencia 2', key:false }),
        new Field({name:'telefono_emergencia_2', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Telefono emergencia 2', key:false }),
        new Field({name:'comentarios', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Comentarios', key:false }),
        new Field({name:'revisado_con_personal', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Revisado con personal', key:false }),
        new Field({name:'revisado_por_rh', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Revisado por rh', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let invoice_types = new Table({
    name: 'invoice_types',
    table: 'invoice_types',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Type', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let jobs = new Table({
    name: 'jobs',
    table: 'jobs',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'queue', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Queue', key:false }),
        new Field({name:'attempts', type: 'tinyint(3) unsigned', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Attempts', key:false }),
        new Field({name:'reserved_at', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Reserved at', key:false }),
        new Field({name:'available_at', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Available at', key:false }),
        new Field({name:'created_at', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Created at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let low_target_justifications = new Table({
    name: 'low_target_justifications',
    table: 'low_target_justifications',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let material_requisitions = new Table({
    name: 'material_requisitions',
    table: 'material_requisitions',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'company_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Company id', key:false }),
        new Field({name:'transaction_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Transaction id', key:false }),
        new Field({name:'order_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Order id', key:false }),
        new Field({name:'cute_ticket', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Cute ticket', key:false }),
        new Field({name:'reason', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Reason', key:false }),
        new Field({name:'movement_type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Movement type', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let menu_entries = new Table({
    name: 'menu_entries',
    table: 'menu_entries',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'order', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Order', key:false }),
        new Field({name:'header_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Header id', key:false }),
        new Field({name:'address', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Address', key:false }),
        new Field({name:'model', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Model', key:false }),
        new Field({name:'mount_point', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Mount point', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let menu_headers = new Table({
    name: 'menu_headers',
    table: 'menu_headers',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Name', key:false }),
        new Field({name:'order', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Order', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let menu_params = new Table({
    name: 'menu_params',
    table: 'menu_params',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'entry_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:true, label:'Entry id', key:false }),
        new Field({name:'key', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Key', key:false }),
        new Field({name:'value_source', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Value source', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let migrations = new Table({
    name: 'migrations',
    table: 'migrations',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'migration', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Migration', key:false }),
        new Field({name:'batch', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Batch', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let movements = new Table({
    name: 'movements',
    table: 'movements',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'qty_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Qty id', key:false }),
        new Field({name:'part_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part id', key:false }),
        new Field({name:'in', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'In', key:false }),
        new Field({name:'out', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'Out', key:false }),
        new Field({name:'location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Location', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'material_requisition_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Material requisition id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let nom_personnel = new Table({
    name: 'nom_personnel',
    table: 'nom_personnel',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'varchar(6)', comment:'', length:6, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'name', type: 'char(50)', comment:'', length:50, columnType:'char', nullable:false, label:'Name', key:false }),
        new Field({name:'last_name_position', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Last name position', key:false }),
        new Field({name:'name_position', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Name position', key:false }),
        new Field({name:'gender', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Gender', key:false }),
        new Field({name:'shift', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Shift', key:false }),
        new Field({name:'contract', type: 'char(2)', comment:'', length:2, columnType:'char', nullable:false, label:'Contract', key:false }),
        new Field({name:'company', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Company', key:false }),
        new Field({name:'payable', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Payable', key:false }),
        new Field({name:'department', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Department', key:false }),
        new Field({name:'position', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Position', key:false }),
        new Field({name:'supervisor', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Supervisor', key:false }),
        new Field({name:'organization', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Organization', key:false }),
        new Field({name:'category', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Category', key:false }),
        new Field({name:'position_2', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Position 2', key:false }),
        new Field({name:'bank', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'Bank', key:false }),
        new Field({name:'payment', type: 'varchar(8)', comment:'', length:8, columnType:'varchar', nullable:false, label:'Payment', key:false }),
        new Field({name:'money_adjustement', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Money adjustement', key:false }),
        new Field({name:'pantry_vouchers_adjustment', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Pantry vouchers adjustment', key:false }),
        new Field({name:'pantry_vouchers_1', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Pantry vouchers 1', key:false }),
        new Field({name:'pantry_vouchers_2', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Pantry vouchers 2', key:false }),
        new Field({name:'hire_date', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'Hire date', key:false }),
        new Field({name:'fired_date', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'Fired date', key:false }),
        new Field({name:'contract_end_date', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'Contract end date', key:false }),
        new Field({name:'reactivation_period_number', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Reactivation period number', key:false }),
        new Field({name:'is_future_fire', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is future fire', key:false }),
        new Field({name:'is_future_hire', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is future hire', key:false }),
        new Field({name:'is_active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is active', key:false }),
        new Field({name:'is_work_union', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is work union', key:false }),
        new Field({name:'payment_method', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Payment method', key:false }),
        new Field({name:'actual_salary_validity', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'Actual salary validity', key:false }),
        new Field({name:'saving_percentage', type: 'decimal(8,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Saving percentage', key:false }),
        new Field({name:'saving_gravable_amount_imss', type: 'decimal(8,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Saving gravable amount imss', key:false }),
        new Field({name:'apply_cas_credit', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Apply cas credit', key:false }),
        new Field({name:'do_we_pay_cas', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Do we pay cas', key:false }),
        new Field({name:'do_we_make_annual_calculation', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Do we make annual calculation', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let oauth_access_tokens = new Table({
    name: 'oauth_access_tokens',
    table: 'oauth_access_tokens',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:false, label:'Id', key:true }),
        new Field({name:'user_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'User id', key:false }),
        new Field({name:'client_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Client id', key:false }),
        new Field({name:'name', type: 'varchar(255)', comment:'', length:255, columnType:'varchar', nullable:true, label:'Name', key:false }),
        new Field({name:'scopes', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Scopes', key:false }),
        new Field({name:'revoked', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Revoked', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'expires_at', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:true, label:'Expires at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let oauth_auth_codes = new Table({
    name: 'oauth_auth_codes',
    table: 'oauth_auth_codes',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:false, label:'Id', key:true }),
        new Field({name:'user_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'client_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Client id', key:false }),
        new Field({name:'scopes', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Scopes', key:false }),
        new Field({name:'revoked', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Revoked', key:false }),
        new Field({name:'expires_at', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:true, label:'Expires at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let oauth_clients = new Table({
    name: 'oauth_clients',
    table: 'oauth_clients',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'user_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'User id', key:false }),
        new Field({name:'name', type: 'varchar(255)', comment:'', length:255, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'secret', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:false, label:'Secret', key:false }),
        new Field({name:'redirect', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Redirect', key:false }),
        new Field({name:'personal_access_client', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Personal access client', key:false }),
        new Field({name:'password_client', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Password client', key:false }),
        new Field({name:'revoked', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Revoked', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let oauth_personal_access_clients = new Table({
    name: 'oauth_personal_access_clients',
    table: 'oauth_personal_access_clients',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'client_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Client id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let oauth_refresh_tokens = new Table({
    name: 'oauth_refresh_tokens',
    table: 'oauth_refresh_tokens',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:false, label:'Id', key:true }),
        new Field({name:'access_token_id', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:false, label:'Access token id', key:false }),
        new Field({name:'revoked', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Revoked', key:false }),
        new Field({name:'expires_at', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:true, label:'Expires at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let organizations = new Table({
    name: 'organizations',
    table: 'organizations',
    connection: 'mysql',
    fields: [
        new Field({name:'code', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Code', key:false }),
        new Field({name:'id', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'cm101', type: 'char(5)', comment:'', length:5, columnType:'char', nullable:false, label:'Cm101', key:false }),
        new Field({name:'cm102', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'Cm102', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let pageview = new Table({
    name: 'pageview',
    table: 'pageview',
    connection: 'mysql',
    fields: [
        new Field({name:'view_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'View id', key:true }),
        new Field({name:'website_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Website id', key:false }),
        new Field({name:'session_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Session id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Created at', key:false }),
        new Field({name:'url', type: 'varchar(500)', comment:'', length:500, columnType:'varchar', nullable:false, label:'Url', key:false }),
        new Field({name:'referrer', type: 'varchar(500)', comment:'', length:500, columnType:'varchar', nullable:true, label:'Referrer', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let parts = new Table({
    name: 'parts',
    table: 'parts',
    connection: 'mysql',
    fields: [
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:true }),
        new Field({name:'part_aka_customer', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part aka customer', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'comments', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Comments', key:false }),
        new Field({name:'company_id', type: 'bigint(20)', comment:'', length:0, columnType:'bigint', nullable:false, label:'Company id', key:false }),
        new Field({name:'type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Type', key:false }),
        new Field({name:'category', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Category', key:false }),
        new Field({name:'division', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Division', key:false }),
        new Field({name:'unit_of_measure_us', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Unit of measure us', key:false }),
        new Field({name:'default_location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Default location', key:false }),
        new Field({name:'unit_of_measure_mx', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Unit of measure mx', key:false }),
        new Field({name:'material_type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Material type', key:false }),
        new Field({name:'origin', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Origin', key:false }),
        new Field({name:'cost_us', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Cost us', key:false }),
        new Field({name:'is_internal', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is internal', key:false }),
        new Field({name:'created_by', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Created by', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let part_counts = new Table({
    name: 'part_counts',
    table: 'part_counts',
    connection: 'mysql',
    fields: [
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:false }),
        new Field({name:'transaction_id', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Transaction id', key:true }),
        new Field({name:'snapshot_qty', type: 'decimal(15,5)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Snapshot qty', key:false }),
        new Field({name:'counted', type: 'decimal(15,5)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Counted', key:false }),
        new Field({name:'adjusted', type: 'decimal(15,5)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Adjusted', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'User id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let part_count_selection = new Table({
    name: 'part_count_selection',
    table: 'part_count_selection',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:false }),
        new Field({name:'warehouse', type: 'decimal(15,4)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Warehouse', key:false }),
        new Field({name:'receiving', type: 'decimal(15,4)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Receiving', key:false }),
        new Field({name:'imp_exp', type: 'decimal(15,4)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Imp exp', key:false }),
        new Field({name:'total', type: 'decimal(15,4)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Total', key:false }),
        new Field({name:'physical_count', type: 'decimal(15,4)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Physical count', key:false }),
        new Field({name:'inclusor', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Inclusor', key:false }),
        new Field({name:'company', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Company', key:false }),
        new Field({name:'is_freeze', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is freeze', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let part_tracker_cumple = new Table({
    name: 'part_tracker_cumple',
    table: 'part_tracker_cumple',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'file_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'File id', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'User id', key:false }),
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part', key:false }),
        new Field({name:'mx_part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Mx part', key:false }),
        new Field({name:'mx_sub_part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Mx sub part', key:false }),
        new Field({name:'desc_cumple_us', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Desc cumple us', key:false }),
        new Field({name:'desc_other', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Desc other', key:false }),
        new Field({name:'origin_mx', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Origin mx', key:false }),
        new Field({name:'um_mx', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Um mx', key:false }),
        new Field({name:'mx_price', type: 'decimal(12,5)', comment:'', length:0, columnType:'decimal', nullable:true, label:'Mx price', key:false }),
        new Field({name:'mx_weight', type: 'decimal(12,5)', comment:'', length:0, columnType:'decimal', nullable:true, label:'Mx weight', key:false }),
        new Field({name:'hts_mx', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Hts mx', key:false }),
        new Field({name:'familia', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Familia', key:false }),
        new Field({name:'codigo', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Codigo', key:false }),
        new Field({name:'tipo_de_materia', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Tipo de materia', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let password_resets = new Table({
    name: 'password_resets',
    table: 'password_resets',
    connection: 'mysql',
    fields: [
        new Field({name:'email', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Email', key:false }),
        new Field({name:'token', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Token', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let personal_tra01 = new Table({
    name: 'personal_tra01',
    table: 'personal_tra01',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Id', key:false }),
        new Field({name:'TR101', type: 'char(6)', comment:'', length:6, columnType:'char', nullable:false, label:'TR101', key:false }),
        new Field({name:'TR102', type: 'char(50)', comment:'', length:50, columnType:'char', nullable:false, label:'TR102', key:false }),
        new Field({name:'TR103', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR103', key:false }),
        new Field({name:'TR104', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR104', key:false }),
        new Field({name:'TR105', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR105', key:false }),
        new Field({name:'TR106', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR106', key:false }),
        new Field({name:'TR107', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR107', key:false }),
        new Field({name:'TR108', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR108', key:false }),
        new Field({name:'TR109', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR109', key:false }),
        new Field({name:'TR110', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR110', key:false }),
        new Field({name:'TR111', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR111', key:false }),
        new Field({name:'TR112', type: 'char(2)', comment:'', length:2, columnType:'char', nullable:false, label:'TR112', key:false }),
        new Field({name:'TR113', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR113', key:false }),
        new Field({name:'TR114', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR114', key:false }),
        new Field({name:'TR115', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR115', key:false }),
        new Field({name:'TR116', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR116', key:false }),
        new Field({name:'TR117', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR117', key:false }),
        new Field({name:'TR118', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR118', key:false }),
        new Field({name:'TR119', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR119', key:false }),
        new Field({name:'TR120', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR120', key:false }),
        new Field({name:'TR121', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR121', key:false }),
        new Field({name:'TR122', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR122', key:false }),
        new Field({name:'TR123', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR123', key:false }),
        new Field({name:'TR124', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR124', key:false }),
        new Field({name:'TR125', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR125', key:false }),
        new Field({name:'TR126', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR126', key:false }),
        new Field({name:'TR127', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'TR127', key:false }),
        new Field({name:'TR128', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'TR128', key:false }),
        new Field({name:'TR129', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'TR129', key:false }),
        new Field({name:'TR130', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR130', key:false }),
        new Field({name:'TR131', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR131', key:false }),
        new Field({name:'TR132', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR132', key:false }),
        new Field({name:'TR133', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR133', key:false }),
        new Field({name:'TR134', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR134', key:false }),
        new Field({name:'TR135', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR135', key:false }),
        new Field({name:'TR136', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'TR136', key:false }),
        new Field({name:'TR137', type: 'decimal(8,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR137', key:false }),
        new Field({name:'TR138', type: 'decimal(8,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR138', key:false }),
        new Field({name:'TR139', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR139', key:false }),
        new Field({name:'TR140', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR140', key:false }),
        new Field({name:'TR141', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR141', key:false }),
        new Field({name:'TR142', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'TR142', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let personal_tra02 = new Table({
    name: 'personal_tra02',
    table: 'personal_tra02',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Id', key:false }),
        new Field({name:'TR201', type: 'char(6)', comment:'', length:6, columnType:'char', nullable:false, label:'TR201', key:false }),
        new Field({name:'TR202', type: 'char(6)', comment:'', length:6, columnType:'char', nullable:false, label:'TR202', key:false }),
        new Field({name:'TR203', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR203', key:false }),
        new Field({name:'TR204', type: 'char(15)', comment:'', length:15, columnType:'char', nullable:false, label:'TR204', key:false }),
        new Field({name:'TR205', type: 'char(18)', comment:'', length:18, columnType:'char', nullable:false, label:'TR205', key:false }),
        new Field({name:'TR206', type: 'char(15)', comment:'', length:15, columnType:'char', nullable:false, label:'TR206', key:false }),
        new Field({name:'TR207', type: 'char(3)', comment:'', length:3, columnType:'char', nullable:false, label:'TR207', key:false }),
        new Field({name:'TR208', type: 'char(16)', comment:'', length:16, columnType:'char', nullable:false, label:'TR208', key:false }),
        new Field({name:'TR209', type: 'char(16)', comment:'', length:16, columnType:'char', nullable:false, label:'TR209', key:false }),
        new Field({name:'TR210', type: 'char(20)', comment:'', length:20, columnType:'char', nullable:false, label:'TR210', key:false }),
        new Field({name:'TR211', type: 'char(20)', comment:'', length:20, columnType:'char', nullable:false, label:'TR211', key:false }),
        new Field({name:'TR212', type: 'decimal(7,4)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR212', key:false }),
        new Field({name:'TR213', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR213', key:false }),
        new Field({name:'TR214', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR214', key:false }),
        new Field({name:'TR215', type: 'char(10)', comment:'', length:10, columnType:'char', nullable:false, label:'TR215', key:false }),
        new Field({name:'TR216', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'TR216', key:false }),
        new Field({name:'TR217', type: 'decimal(8,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR217', key:false }),
        new Field({name:'TR218', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR218', key:false }),
        new Field({name:'TR219', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR219', key:false }),
        new Field({name:'TR220', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR220', key:false }),
        new Field({name:'TR221', type: 'decimal(5,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR221', key:false }),
        new Field({name:'TR222', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR222', key:false }),
        new Field({name:'TR223', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'TR223', key:false }),
        new Field({name:'TR224', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'TR224', key:false }),
        new Field({name:'TR225', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'TR225', key:false }),
        new Field({name:'TR226', type: 'char(20)', comment:'', length:20, columnType:'char', nullable:false, label:'TR226', key:false }),
        new Field({name:'TR227', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR227', key:false }),
        new Field({name:'TR228', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR228', key:false }),
        new Field({name:'TR229', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR229', key:false }),
        new Field({name:'TR230', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR230', key:false }),
        new Field({name:'TR231', type: 'decimal(3,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR231', key:false }),
        new Field({name:'TR232', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR232', key:false }),
        new Field({name:'TR233', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR233', key:false }),
        new Field({name:'TR234', type: 'decimal(8,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR234', key:false }),
        new Field({name:'TR235', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'TR235', key:false }),
        new Field({name:'TR236', type: 'char(20)', comment:'', length:20, columnType:'char', nullable:false, label:'TR236', key:false }),
        new Field({name:'TR237', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR237', key:false }),
        new Field({name:'TR238', type: 'char(80)', comment:'', length:80, columnType:'char', nullable:false, label:'TR238', key:false }),
        new Field({name:'TR239', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR239', key:false }),
        new Field({name:'TR240', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR240', key:false }),
        new Field({name:'TR241', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR241', key:false }),
        new Field({name:'TR242', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR242', key:false }),
        new Field({name:'TR243', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR243', key:false }),
        new Field({name:'TR244', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR244', key:false }),
        new Field({name:'TR245', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR245', key:false }),
        new Field({name:'TR246', type: 'decimal(4,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR246', key:false }),
        new Field({name:'TR247', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR247', key:false }),
        new Field({name:'TR248', type: 'char(150)', comment:'', length:150, columnType:'char', nullable:false, label:'TR248', key:false }),
        new Field({name:'TR249', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR249', key:false }),
        new Field({name:'TR250', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'TR250', key:false }),
        new Field({name:'TR251', type: 'char(20)', comment:'', length:20, columnType:'char', nullable:false, label:'TR251', key:false }),
        new Field({name:'TR252', type: 'decimal(2,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR252', key:false }),
        new Field({name:'TR253', type: 'char(80)', comment:'', length:80, columnType:'char', nullable:false, label:'TR253', key:false }),
        new Field({name:'TR254', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'TR254', key:false }),
        new Field({name:'TR255', type: 'char(15)', comment:'', length:15, columnType:'char', nullable:false, label:'TR255', key:false }),
        new Field({name:'TR256', type: 'decimal(7,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR256', key:false }),
        new Field({name:'TR257', type: 'char(15)', comment:'', length:15, columnType:'char', nullable:false, label:'TR257', key:false }),
        new Field({name:'TR258', type: 'decimal(7,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR258', key:false }),
        new Field({name:'TR259', type: 'char(15)', comment:'', length:15, columnType:'char', nullable:false, label:'TR259', key:false }),
        new Field({name:'TR260', type: 'decimal(7,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR260', key:false }),
        new Field({name:'TR261', type: 'char(15)', comment:'', length:15, columnType:'char', nullable:false, label:'TR261', key:false }),
        new Field({name:'TR262', type: 'decimal(7,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR262', key:false }),
        new Field({name:'TR263', type: 'char(15)', comment:'', length:15, columnType:'char', nullable:false, label:'TR263', key:false }),
        new Field({name:'TR264', type: 'decimal(7,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR264', key:false }),
        new Field({name:'TR265', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR265', key:false }),
        new Field({name:'TR266', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'TR266', key:false }),
        new Field({name:'TR267', type: 'char(4)', comment:'', length:4, columnType:'char', nullable:false, label:'TR267', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let personnel = new Table({
    name: 'personnel',
    table: 'personnel',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'varchar(6)', comment:'', length:6, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'name', type: 'char(50)', comment:'', length:50, columnType:'char', nullable:false, label:'Name', key:false }),
        new Field({name:'gender', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Gender', key:false }),
        new Field({name:'shift', type: 'decimal(1,0)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Shift', key:false }),
        new Field({name:'bar_code', type: 'int(1)', comment:'', length:0, columnType:'int', nullable:false, label:'Bar code', key:false }),
        new Field({name:'contract', type: 'char(2)', comment:'', length:2, columnType:'char', nullable:false, label:'Contract', key:false }),
        new Field({name:'company', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Company', key:false }),
        new Field({name:'department', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Department', key:false }),
        new Field({name:'position', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Position', key:false }),
        new Field({name:'supervisor', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Supervisor', key:false }),
        new Field({name:'organization', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:true, label:'Organization', key:false }),
        new Field({name:'birth_date', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:false, label:'Birth date', key:false }),
        new Field({name:'hire_date', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'Hire date', key:false }),
        new Field({name:'is_active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is active', key:false }),
        new Field({name:'is_work_union', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is work union', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let phone_extensions = new Table({
    name: 'phone_extensions',
    table: 'phone_extensions',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'extension', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Extension', key:false }),
        new Field({name:'first_name', type: 'varchar(200)', comment:'', length:200, columnType:'varchar', nullable:false, label:'First name', key:false }),
        new Field({name:'last_name', type: 'varchar(200)', comment:'', length:200, columnType:'varchar', nullable:true, label:'Last name', key:false }),
        new Field({name:'kind', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:false, label:'Kind', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let positions = new Table({
    name: 'positions',
    table: 'positions',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let positions_master = new Table({
    name: 'positions_master',
    table: 'positions_master',
    connection: 'mysql',
    fields: [
        new Field({name:'code', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Code', key:false }),
        new Field({name:'id', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'cm101', type: 'char(5)', comment:'', length:5, columnType:'char', nullable:false, label:'Cm101', key:false }),
        new Field({name:'cm102', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'Cm102', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let processes = new Table({
    name: 'processes',
    table: 'processes',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let production_records = new Table({
    name: 'production_records',
    table: 'production_records',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'product_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Product id', key:false }),
        new Field({name:'process_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Process id', key:false }),
        new Field({name:'quantity', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'Quantity', key:false }),
        new Field({name:'process_time', type: 'double', comment:'', length:0, columnType:'double', nullable:true, label:'Process time', key:false }),
        new Field({name:'comment', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Comment', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let production_records_non_conform = new Table({
    name: 'production_records_non_conform',
    table: 'production_records_non_conform',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'product_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Product id', key:false }),
        new Field({name:'process_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Process id', key:false }),
        new Field({name:'quantity', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'Quantity', key:false }),
        new Field({name:'process_time', type: 'double', comment:'', length:0, columnType:'double', nullable:true, label:'Process time', key:false }),
        new Field({name:'comment', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Comment', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let products = new Table({
    name: 'products',
    table: 'products',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let product_process_time = new Table({
    name: 'product_process_time',
    table: 'product_process_time',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'company_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:true, label:'Company id', key:false }),
        new Field({name:'product_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Product id', key:false }),
        new Field({name:'process_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Process id', key:false }),
        new Field({name:'time', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'Time', key:false }),
        new Field({name:'comment', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Comment', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let raw_clock_movements = new Table({
    name: 'raw_clock_movements',
    table: 'raw_clock_movements',
    connection: 'mysql',
    fields: [
        new Field({name:'kid', type: 'bigint(19)', comment:'', length:0, columnType:'bigint', nullable:false, label:'Kid', key:true }),
        new Field({name:'id', type: 'char(8)', comment:'', length:8, columnType:'char', nullable:false, label:'Id', key:false }),
        new Field({name:'station', type: 'char(2)', comment:'', length:2, columnType:'char', nullable:true, label:'Station', key:false }),
        new Field({name:'check_date', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:false, label:'Check date', key:false }),
        new Field({name:'created_at', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:true, label:'Updated at', key:false }),
        new Field({name:'meta', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Meta', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let reloj_companies = new Table({
    name: 'reloj_companies',
    table: 'reloj_companies',
    connection: 'mysql',
    fields: [
        new Field({name:'code', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:true, label:'Code', key:false }),
        new Field({name:'id', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:true, label:'Id', key:false }),
        new Field({name:'cm101', type: 'char(5)', comment:'', length:5, columnType:'char', nullable:false, label:'Cm101', key:false }),
        new Field({name:'cm102', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'Cm102', key:false }),
        new Field({name:'order', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Order', key:false }),
        new Field({name:'active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'pix_prefix', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:false, label:'Pix prefix', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let roles = new Table({
    name: 'roles',
    table: 'roles',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'company_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Company id', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let role_user = new Table({
    name: 'role_user',
    table: 'role_user',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'role_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Role id', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let scheduled_absences = new Table({
    name: 'scheduled_absences',
    table: 'scheduled_absences',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'employee_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Employee id', key:false }),
        new Field({name:'tracking_code_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Tracking code id', key:false }),
        new Field({name:'start', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'Start', key:false }),
        new Field({name:'end', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'End', key:false }),
        new Field({name:'active', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Active', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutts_parts = new Table({
    name: 'schutts_parts',
    table: 'schutts_parts',
    connection: 'mysql',
    fields: [
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:true }),
        new Field({name:'part_in_pix', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part in pix', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Description', key:false }),
        new Field({name:'description_client', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Description client', key:false }),
        new Field({name:'unit_of_measure', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Unit of measure', key:false }),
        new Field({name:'unit_of_measure_client', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Unit of measure client', key:false }),
        new Field({name:'min_stock', type: 'decimal(15,0)', comment:'', length:0, columnType:'decimal', nullable:true, label:'Min stock', key:false }),
        new Field({name:'part_type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part type', key:false }),
        new Field({name:'max_stock', type: 'decimal(15,0)', comment:'', length:0, columnType:'decimal', nullable:true, label:'Max stock', key:false }),
        new Field({name:'manufacturer', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Manufacturer', key:false }),
        new Field({name:'regimen', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Regimen', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'is_internal', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Is internal', key:false }),
        new Field({name:'created_by', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Created by', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutts_parts_back = new Table({
    name: 'schutts_parts_back',
    table: 'schutts_parts_back',
    connection: 'mysql',
    fields: [
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:true }),
        new Field({name:'part_in_catalog', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part in catalog', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'unit_of_measure', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Unit of measure', key:false }),
        new Field({name:'part_type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part type', key:false }),
        new Field({name:'manufacturer', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Manufacturer', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'is_internal', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is internal', key:false }),
        new Field({name:'created_by', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Created by', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutts_parts_back2 = new Table({
    name: 'schutts_parts_back2',
    table: 'schutts_parts_back2',
    connection: 'mysql',
    fields: [
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:true }),
        new Field({name:'part_in_catalog', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part in catalog', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Description', key:false }),
        new Field({name:'unit_of_measure', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Unit of measure', key:false }),
        new Field({name:'part_type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part type', key:false }),
        new Field({name:'manufacturer', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Manufacturer', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'is_internal', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Is internal', key:false }),
        new Field({name:'created_by', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Created by', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutt_formulas = new Table({
    name: 'schutt_formulas',
    table: 'schutt_formulas',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'system', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'System', key:false }),
        new Field({name:'color_code', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Color code', key:false }),
        new Field({name:'color_number', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Color number', key:false }),
        new Field({name:'layer', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Layer', key:false }),
        new Field({name:'version', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Version', key:false }),
        new Field({name:'revision_date', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Revision date', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutt_paint_part_master = new Table({
    name: 'schutt_paint_part_master',
    table: 'schutt_paint_part_master',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'schutt_paint_code', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Schutt paint code', key:false }),
        new Field({name:'ie_part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Ie part', key:false }),
        new Field({name:'ie_uom', type: 'varchar(10)', comment:'', length:10, columnType:'varchar', nullable:true, label:'Ie uom', key:false }),
        new Field({name:'schutt_uom', type: 'varchar(10)', comment:'', length:10, columnType:'varchar', nullable:true, label:'Schutt uom', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutt_paint_uses = new Table({
    name: 'schutt_paint_uses',
    table: 'schutt_paint_uses',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'order', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Order', key:false }),
        new Field({name:'quantity', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Quantity', key:false }),
        new Field({name:'color_code', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Color code', key:false }),
        new Field({name:'color', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Color', key:false }),
        new Field({name:'finishing', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Finishing', key:false }),
        new Field({name:'is_rework', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Is rework', key:false }),
        new Field({name:'captured_in_ht_report', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Captured in ht report', key:false }),
        new Field({name:'paint_in_gr', type: 'double(8,2)', comment:'', length:0, columnType:'double', nullable:true, label:'Paint in gr', key:false }),
        new Field({name:'oven_entry_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Oven entry at', key:false }),
        new Field({name:'oven_out_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Oven out at', key:false }),
        new Field({name:'oven_length', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Oven length', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutt_part_transactions = new Table({
    name: 'schutt_part_transactions',
    table: 'schutt_part_transactions',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:false }),
        new Field({name:'in', type: 'decimal(15,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'In', key:false }),
        new Field({name:'out', type: 'decimal(15,3)', comment:'', length:0, columnType:'decimal', nullable:true, label:'Out', key:false }),
        new Field({name:'location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Location', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'temp_loc', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:true, label:'Temp loc', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutt_part_transactions_back = new Table({
    name: 'schutt_part_transactions_back',
    table: 'schutt_part_transactions_back',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:false }),
        new Field({name:'in', type: 'decimal(15,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'In', key:false }),
        new Field({name:'out', type: 'decimal(15,3)', comment:'', length:0, columnType:'decimal', nullable:true, label:'Out', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Location', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutt_products = new Table({
    name: 'schutt_products',
    table: 'schutt_products',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'number', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Number', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'short_description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Short description', key:false }),
        new Field({name:'voc', type: 'decimal(12,4)', comment:'', length:0, columnType:'decimal', nullable:true, label:'Voc', key:false }),
        new Field({name:'density', type: 'decimal(12,4)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Density', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let schutt_product_percentages = new Table({
    name: 'schutt_product_percentages',
    table: 'schutt_product_percentages',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'schutt_formula_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Schutt formula id', key:false }),
        new Field({name:'schutt_product_number', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Schutt product number', key:false }),
        new Field({name:'value', type: 'decimal(12,2)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Value', key:false }),
        new Field({name:'grams', type: 'decimal(12,2)', comment:'', length:0, columnType:'decimal', nullable:true, label:'Grams', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let screen_stats = new Table({
    name: 'screen_stats',
    table: 'screen_stats',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'User id', key:false }),
        new Field({name:'route', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Route', key:false }),
        new Field({name:'width', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Width', key:false }),
        new Field({name:'height', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Height', key:false }),
        new Field({name:'x', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'X', key:false }),
        new Field({name:'y', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Y', key:false }),
        new Field({name:'inner_height', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Inner height', key:false }),
        new Field({name:'outer_height', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Outer height', key:false }),
        new Field({name:'inner_width', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Inner width', key:false }),
        new Field({name:'outer_width', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:true, label:'Outer width', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let session = new Table({
    name: 'session',
    table: 'session',
    connection: 'mysql',
    fields: [
        new Field({name:'session_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Session id', key:true }),
        new Field({name:'session_uuid', type: 'varchar(36)', comment:'', length:36, columnType:'varchar', nullable:false, label:'Session uuid', key:false }),
        new Field({name:'website_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Website id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Created at', key:false }),
        new Field({name:'hostname', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:true, label:'Hostname', key:false }),
        new Field({name:'browser', type: 'varchar(20)', comment:'', length:20, columnType:'varchar', nullable:true, label:'Browser', key:false }),
        new Field({name:'os', type: 'varchar(20)', comment:'', length:20, columnType:'varchar', nullable:true, label:'Os', key:false }),
        new Field({name:'device', type: 'varchar(20)', comment:'', length:20, columnType:'varchar', nullable:true, label:'Device', key:false }),
        new Field({name:'screen', type: 'varchar(11)', comment:'', length:11, columnType:'varchar', nullable:true, label:'Screen', key:false }),
        new Field({name:'language', type: 'varchar(35)', comment:'', length:35, columnType:'varchar', nullable:true, label:'Language', key:false }),
        new Field({name:'country', type: 'char(2)', comment:'', length:2, columnType:'char', nullable:true, label:'Country', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let shifts = new Table({
    name: 'shifts',
    table: 'shifts',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'start', type: 'time', comment:'', length:0, columnType:'time', nullable:false, label:'Start', key:false }),
        new Field({name:'end', type: 'time', comment:'', length:0, columnType:'time', nullable:false, label:'End', key:false }),
        new Field({name:'duration', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'Duration', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let shipments = new Table({
    name: 'shipments',
    table: 'shipments',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'loading_deck_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Loading deck id', key:false }),
        new Field({name:'couchdb_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Couchdb id', key:false }),
        new Field({name:'company_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:true, label:'Company id', key:false }),
        new Field({name:'invoice_type_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:true, label:'Invoice type id', key:false }),
        new Field({name:'broker_agency_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:true, label:'Broker agency id', key:false }),
        new Field({name:'driver_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:true, label:'Driver id', key:false }),
        new Field({name:'carrier_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:true, label:'Carrier id', key:false }),
        new Field({name:'freight_box_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:true, label:'Freight box id', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let supervisors = new Table({
    name: 'supervisors',
    table: 'supervisors',
    connection: 'mysql',
    fields: [
        new Field({name:'code', type: 'varchar(1)', comment:'', length:1, columnType:'varchar', nullable:false, label:'Code', key:false }),
        new Field({name:'id', type: 'varchar(4)', comment:'', length:4, columnType:'varchar', nullable:false, label:'Id', key:false }),
        new Field({name:'cm101', type: 'char(5)', comment:'', length:5, columnType:'char', nullable:false, label:'Cm101', key:false }),
        new Field({name:'cm102', type: 'char(40)', comment:'', length:40, columnType:'char', nullable:false, label:'Cm102', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let telescope_entries = new Table({
    name: 'telescope_entries',
    table: 'telescope_entries',
    connection: 'mysql',
    fields: [
        new Field({name:'sequence', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Sequence', key:true }),
        new Field({name:'uuid', type: 'char(36)', comment:'', length:36, columnType:'char', nullable:false, label:'Uuid', key:false }),
        new Field({name:'batch_id', type: 'char(36)', comment:'', length:36, columnType:'char', nullable:false, label:'Batch id', key:false }),
        new Field({name:'family_hash', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Family hash', key:false }),
        new Field({name:'should_display_on_index', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Should display on index', key:false }),
        new Field({name:'type', type: 'varchar(20)', comment:'', length:20, columnType:'varchar', nullable:false, label:'Type', key:false }),
        new Field({name:'content', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Content', key:false }),
        new Field({name:'created_at', type: 'datetime', comment:'', length:0, columnType:'datetime', nullable:true, label:'Created at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let telescope_entries_tags = new Table({
    name: 'telescope_entries_tags',
    table: 'telescope_entries_tags',
    connection: 'mysql',
    fields: [
        new Field({name:'entry_uuid', type: 'char(36)', comment:'', length:36, columnType:'char', nullable:false, label:'Entry uuid', key:false }),
        new Field({name:'tag', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Tag', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let telescope_monitoring = new Table({
    name: 'telescope_monitoring',
    table: 'telescope_monitoring',
    connection: 'mysql',
    fields: [
        new Field({name:'tag', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Tag', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let time_tracking = new Table({
    name: 'time_tracking',
    table: 'time_tracking',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'employee_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Employee id', key:false }),
        new Field({name:'tracking_code_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Tracking code id', key:false }),
        new Field({name:'check_out', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Check out', key:false }),
        new Field({name:'check_in', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Check in', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let tracking_codes = new Table({
    name: 'tracking_codes',
    table: 'tracking_codes',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let transactions_toucheds = new Table({
    name: 'transactions_toucheds',
    table: 'transactions_toucheds',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'transaction', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Transaction', key:false }),
        new Field({name:'location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Location', key:false }),
        new Field({name:'part_affecting', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part affecting', key:false }),
        new Field({name:'is_frozen', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is frozen', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let tyco_parts = new Table({
    name: 'tyco_parts',
    table: 'tyco_parts',
    connection: 'mysql',
    fields: [
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:true }),
        new Field({name:'part_in_catalog', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Part in catalog', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'unit_of_measure', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:true, label:'Unit of measure', key:false }),
        new Field({name:'part_type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part type', key:false }),
        new Field({name:'manufacturer', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Manufacturer', key:false }),
        new Field({name:'is_internal', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is internal', key:false }),
        new Field({name:'created_by', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Created by', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let tyco_part_transactions = new Table({
    name: 'tyco_part_transactions',
    table: 'tyco_part_transactions',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'part', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part', key:false }),
        new Field({name:'in', type: 'decimal(15,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'In', key:false }),
        new Field({name:'out', type: 'decimal(15,3)', comment:'', length:0, columnType:'decimal', nullable:false, label:'Out', key:false }),
        new Field({name:'location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Location', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let unavailable_time_tracking = new Table({
    name: 'unavailable_time_tracking',
    table: 'unavailable_time_tracking',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'employee_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Employee id', key:false }),
        new Field({name:'tracking_code_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Tracking code id', key:false }),
        new Field({name:'time_check', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Time check', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let users = new Table({
    name: 'users',
    table: 'users',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'email', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Email', key:false }),
        new Field({name:'username', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:true, label:'Username', key:false }),
        new Field({name:'password', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Password', key:false }),
        new Field({name:'remember_token', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:true, label:'Remember token', key:false }),
        new Field({name:'api_token', type: 'varchar(60)', comment:'', length:60, columnType:'varchar', nullable:true, label:'Api token', key:false }),
        new Field({name:'company_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:true, label:'Company id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
        new Field({name:'email_verified_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Email verified at', key:false }),
        new Field({name:'is_admin', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:true, label:'Is admin', key:false }),
        new Field({name:'deleted_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Deleted at', key:false }),
        new Field({name:'comments', type: 'text', comment:'', length:65535, columnType:'text', nullable:true, label:'Comments', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let visual_help_files = new Table({
    name: 'visual_help_files',
    table: 'visual_help_files',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'company_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Company id', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'file_path', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'File path', key:false }),
        new Field({name:'original_name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Original name', key:false }),
        new Field({name:'retrieval_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Retrieval id', key:false }),
        new Field({name:'drawing', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Drawing', key:false }),
        new Field({name:'type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Type', key:false }),
        new Field({name:'extension', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Extension', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let website = new Table({
    name: 'website',
    table: 'website',
    connection: 'mysql',
    fields: [
        new Field({name:'website_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Website id', key:true }),
        new Field({name:'website_uuid', type: 'varchar(36)', comment:'', length:36, columnType:'varchar', nullable:false, label:'Website uuid', key:false }),
        new Field({name:'user_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'User id', key:false }),
        new Field({name:'name', type: 'varchar(100)', comment:'', length:100, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'domain', type: 'varchar(500)', comment:'', length:500, columnType:'varchar', nullable:true, label:'Domain', key:false }),
        new Field({name:'share_id', type: 'varchar(64)', comment:'', length:64, columnType:'varchar', nullable:true, label:'Share id', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:false, label:'Created at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let work_order_request = new Table({
    name: 'work_order_request',
    table: 'work_order_request',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Id', key:true }),
        new Field({name:'creator_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Creator id', key:false }),
        new Field({name:'division_id', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Division id', key:false }),
        new Field({name:'is_in_ftp', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Is in ftp', key:false }),
        new Field({name:'product', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Product', key:false }),
        new Field({name:'number_of_sections', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Number of sections', key:false }),
        new Field({name:'description', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Description', key:false }),
        new Field({name:'shipping_address', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Shipping address', key:false }),
        new Field({name:'attn_to', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Attn to', key:false }),
        new Field({name:'accept_overtime', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Accept overtime', key:false }),
        new Field({name:'expedite_order', type: 'tinyint(1)', comment:'', length:0, columnType:'tinyint', nullable:false, label:'Expedite order', key:false }),
        new Field({name:'required_date', type: 'date', comment:'', length:0, columnType:'date', nullable:false, label:'Required date', key:false }),
        new Field({name:'commitment_date', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'Commitment date', key:false }),
        new Field({name:'comments', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Comments', key:false }),
        new Field({name:'state', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'State', key:false }),
        new Field({name:'schedule_date', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'Schedule date', key:false }),
        new Field({name:'production_date', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'Production date', key:false }),
        new Field({name:'shipped_date', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'Shipped date', key:false }),
        new Field({name:'received_date', type: 'date', comment:'', length:0, columnType:'date', nullable:true, label:'Received date', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let bill_of_materials = new Table({
    name: 'bill_of_materials',
    table: 'bill_of_materials',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'mfg_center', type: 'bigint(20)', comment:'', length:0, columnType:'bigint', nullable:false, label:'Mfg center', key:false }),
        new Field({name:'mfg_area', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Mfg area', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'revision', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Revision', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'labor', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'Labor', key:false }),
        new Field({name:'status', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Status', key:false }),
        new Field({name:'flag', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Flag', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let bom_lists = new Table({
    name: 'bom_lists',
    table: 'bom_lists',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'bom_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Bom id', key:false }),
        new Field({name:'assy_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Assy id', key:false }),
        new Field({name:'part_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Part id', key:false }),
        new Field({name:'route_step_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Route step id', key:false }),
        new Field({name:'flag', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Flag', key:false }),
        new Field({name:'quantity', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'Quantity', key:false }),
        new Field({name:'unit_of_measure', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Unit of measure', key:false }),
        new Field({name:'lot_quentity', type: 'double', comment:'', length:0, columnType:'double', nullable:false, label:'Lot quentity', key:false }),
        new Field({name:'group', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Group', key:false }),
        new Field({name:'status', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Status', key:false }),
        new Field({name:'view_code', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'View code', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let route_definitions = new Table({
    name: 'route_definitions',
    table: 'route_definitions',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'routing_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Routing id', key:false }),
        new Field({name:'step_seq', type: 'int(11)', comment:'', length:0, columnType:'int', nullable:false, label:'Step seq', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'yield', type: 'decimal(8', comment:'2)', length:0, columnType:'decimal', nullable:false, label:'Yield', key:false }),
        new Field({name:'sam', type: 'decimal(8', comment:'2)', length:0, columnType:'decimal', nullable:false, label:'Sam', key:false }),
        new Field({name:'cycle_time', type: 'decimal(8', comment:'2)', length:0, columnType:'decimal', nullable:false, label:'Cycle time', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let routings = new Table({
    name: 'routings',
    table: 'routings',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'mfg_center', type: 'bigint(20)', comment:'', length:0, columnType:'bigint', nullable:false, label:'Mfg center', key:false }),
        new Field({name:'mfg_area', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Mfg area', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'revision', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Revision', key:false }),
        new Field({name:'description', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Description', key:false }),
        new Field({name:'status', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Status', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let signages = new Table({
    name: 'signages',
    table: 'signages',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'api_id', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Api id', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'location', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Location', key:false }),
        new Field({name:'theme', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Theme', key:false }),
        new Field({name:'cicles_before_refresh', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Cicles before refresh', key:false }),
        new Field({name:'use_syntax_highlight', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Use syntax highlight', key:false }),
        new Field({name:'use_markdown', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Use markdown', key:false }),
        new Field({name:'transition_style', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Transition style', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
let signage_slides = new Table({
    name: 'signage_slides',
    table: 'signage_slides',
    connection: 'mysql',
    fields: [
        new Field({name:'id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Id', key:true }),
        new Field({name:'signage_id', type: 'bigint(20) unsigned', comment:'', length:0, columnType:'bigint', nullable:false, label:'Signage id', key:false }),
        new Field({name:'name', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Name', key:false }),
        new Field({name:'description', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Description', key:false }),
        new Field({name:'duration', type: 'int(10) unsigned', comment:'', length:0, columnType:'int', nullable:false, label:'Duration', key:false }),
        new Field({name:'transition', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Transition', key:false }),
        new Field({name:'asset_type', type: 'varchar(191)', comment:'', length:191, columnType:'varchar', nullable:false, label:'Asset type', key:false }),
        new Field({name:'content', type: 'text', comment:'', length:65535, columnType:'text', nullable:false, label:'Content', key:false }),
        new Field({name:'created_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Created at', key:false }),
        new Field({name:'updated_at', type: 'timestamp', comment:'', length:0, columnType:'timestamp', nullable:true, label:'Updated at', key:false }),
    ],
    plugins:[
        
    ],
    relations:[]
    })
    
    
/** @type {Table[]} */
let allTables = [
absent_people,
account,
activities,
areas,
assid,
breaks_schedule,
break_employee,
broker_agencies,
carriers,
category,
change_records,
clients,
clock_movements,
companies,
companies_reloj,
company_invoices,
company_user,
courses,
courses_takens,
cumple_file_meta,
departments,
digital_signages,
divisions,
drivers,
d_b_f_directories,
d_b_f_files,
d_files,
d_folders,
employees,
employees_not_checking_entrance,
employee_shift,
event,
failed_jobs,
failure_modes,
fields,
files,
freight_boxes,
groups,
group_user,
hartland_parts,
hartland_part_transactions,
health_check_pings,
health_check_services,
informacion_personals,
invoice_types,
jobs,
low_target_justifications,
material_requisitions,
menu_entries,
menu_headers,
menu_params,
migrations,
movements,
nom_personnel,
oauth_access_tokens,
oauth_auth_codes,
oauth_clients,
oauth_personal_access_clients,
oauth_refresh_tokens,
organizations,
pageview,
parts,
part_counts,
part_count_selection,
part_tracker_cumple,
password_resets,
personal_tra01,
personal_tra02,
personnel,
phone_extensions,
positions,
positions_master,
processes,
production_records,
production_records_non_conform,
products,
product_process_time,
raw_clock_movements,
reloj_companies,
roles,
role_user,
scheduled_absences,
schutts_parts,
schutts_parts_back,
schutts_parts_back2,
schutt_formulas,
schutt_paint_part_master,
schutt_paint_uses,
schutt_part_transactions,
schutt_part_transactions_back,
schutt_products,
schutt_product_percentages,
screen_stats,
session,
shifts,
shipments,
supervisors,
telescope_entries,
telescope_entries_tags,
telescope_monitoring,
time_tracking,
tracking_codes,
transactions_toucheds,
tyco_parts,
tyco_part_transactions,
unavailable_time_tracking,
users,
visual_help_files,
website,
work_order_request,
bill_of_materials,
bom_lists,
route_definitions,
routings,
signages,
signage_slides
]
export {allTables}