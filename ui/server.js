import * as _io from 'socket.io'
import express from 'express'
import { createServer } from 'http'
import path from 'path'
import { getDatabaseObjects, pool } from '../src/databaseObjects.js'
import { saveProject, readProject, directoryContent } from '../src/projectIO.js'

let app = express()
let http = createServer(app)
let io = new _io.Server(http, {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204,
    maxAge: 240,
    credentials: true,

})
let __dirname = path.resolve(path.dirname(''))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/ui/index.html')
})

io.on('connection', socket => {
    console.log('a user connected')

    socket.on('chat message', (msg) => {
        io.emit('chat message', msg);
        console.log(msg)
    });

    socket.on('request_config', (msg) => {
        getDatabaseObjects().then(d => {
            console.log('emited columns')
            io.emit('config', JSON.stringify(d));
        }).catch(e => {
            console.log(e)
        })
    });

    socket.on('save_project', (project) => {
        saveProject(project)
    });

    socket.on('get_project', (project) => {
        io.emit('project_contents', readProject(project))
    })

    socket.on('get_project_list', () => {
        io.emit('project_list', directoryContent())
    })

    socket.on('disconnect', () => {
        console.log('user disconnected')
    })
})

http.listen(3001, () => {
    console.log('listening on *:3001')
})

async function handleShutdownGracefully() {
    if (pool) { pool.end().then(() => { console.log('Poll closed') }) }
    return true
}
// process.on("SIGTERM", handleShutdownGracefully);
process.on("SIGINT", handleShutdownGracefully);
// process.on("SIGHUP", handleShutdownGracefully)