// @ts-check
// https://github.com/jondot/hygen/blob/master/src/render.ts
// https://github.com/npm/ignore-walk

const undasherize = str =>
    str
        .split(/[-_]/g)
        .map(w => w[0].toUpperCase() + w.slice(1).toLowerCase())
        .join('')


const capitalize = (string='null') =>
    string
        .charAt(0)
        .toUpperCase() + string.slice(1)


const withFirstUpper = (str) => str.charAt(0).toUpperCase() + str.substring(1)

const withNewlineEnsured = (str) => str + (str.endsWith("\n") ? "" : "\n")

/**
 * 
 * @param {string[]|array[]} nestedString 
 */
const asString = (nestedString) => Array.isArray(nestedString)
    ? nestedString.flat(Infinity)
        .map(withNewlineEnsured)
        .join("")
    : withNewlineEnsured(nestedString)

const indent = (indentLevel) => {
    const indentationPrefix = "    ".repeat(indentLevel)
    const indentLine = (str) => indentationPrefix + str
    return (nestedString) => Array.isArray(nestedString)
        ? nestedString.flat(Infinity).map(indentLine)
        : indentLine(nestedString)
}

function titleCase() {
    // Title Case
}
function upperCase() {
    // UPPERCASE
}
function lowerCase() {
    // lowercase
}
function SwapCase() {
    // lOWERcASE
}
function snakeCase() {
    // snake_case
}
const camelCase = (str) => str
    // camelCase
    .toLowerCase()
    .replace(/ ([a-z])/g, (_, ch) => ch.toUpperCase())
    .replace(" ", "")
function pascalCase() {
    // PascalCase
}
function dotCase() {
    // dotCase.case
}
function dashCase() {
    // dash-case
}
/**
 * Changes dashes and underscores and put spaces instead
 * @param {string} str 
 * @returns {string}
 */
function separateWords(str = '') {
    // ('separeteWords') => 'separate words' 
    return str
    .split(/[-_]/g)
    .join(' ')
}
function separateWithDash() {
    // ('separeteWithDash') => 'separate_with_dashes'
}

export {
    undasherize,
    capitalize,
    camelCase,
    withFirstUpper,
    asString,
    indent,
    separateWords,
}