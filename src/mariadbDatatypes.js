//@ts-check
import { separateWords, capitalize, undasherize } from './helpers.js'

/** */
class Relationship {
    constructor({
        type,
        origin,
        target,

    }) { }
}


/**
 * @typedef {object} TableConstructor
 * @property {string} table
 * @property {string} name
 * @property {Field[]} fields
 * @property {string} [connection = 42 ]
 * @property {string} [primaryKey = 'id']
 * @property {string} [outputFolder = '']
 * @property {boolean} [incrementing]
 * @property {boolean} [timestamps]
 * @property {object} [createdAt]
 * @property {object} [updated_at]
 * @property {boolean} [visible]
 * @property {string[]} [hidden]
 * @property {object} [casts]
 * @property {string[]} [fillable]
 * @property {boolean} [softDeletes]
 * @property {array} [plugins]
 * @property {array} [relations]
 */

class Table {
    /**
     * 
     * @param {TableConstructor} configurationObject 
     */
    constructor({
        name,
        table,
        fields = [],
        connection = 'mysql',
        primaryKey = 'id',
        incrementing = false,
        timestamps = true,
        createdAt,
        updated_at,
        visible,
        hidden,
        casts,
        fillable,
        outputFolder = 'InMotion',
        softDeletes = false,
        relations = []
    }) {
        this.name = name
        this.table = table
        this.fields = fields
        this.connection = connection
        this._primaryKey = primaryKey
        this.incrementing = incrementing
        this.timestamps = timestamps
        this._fillable = fillable
        this.createdAt = createdAt
        this.updated_at = updated_at
        this.visible = visible
        this.hidden = hidden
        this.casts = casts
        this.outputFolder = outputFolder
        this.softDeletes = softDeletes
        this.relations = relations
    }
    /** @returns {string[]} */
    get fillable() {
        return this._fillable
            ? this._fillable
            : this.fields.map(f => f.name)
    }
    /** @returns {string} */
    get fileName() {
        return this.className
    }
    /** @returns {string} */
    get className() {
        return capitalize(undasherize(this.name))
    }
    /** @returns {Field[]} */
    get dateFields() {
        return this.fields.filter(c => ['datetime', 'date', 'timestamp'].includes(c.columnType))
    }
    /** @returns {Field[]} */
    get validations() {
        return this.fields.filter(c => c.nullable)
    }
    /** @returns {boolean} */
    get hasDateFields() {
        return this.dateFields.length > 0
    }
    /** @returns {boolean} */
    get hasSoftDeletes() {
        return this.fields.filter(c => c.name === 'deleted_at').length > 0
    }
    get classUseStatement() {
        return 'use ' + this.classNameSpace + '\\' + this.className + ';'
    }
    get classNameSpace() {
        return ('/App/Models/' + this.outputFolder)
            .split(/[\\\/]/g)
            .filter(d => d)
            .join('\\')
    }
    get controllerNameSpace() {
        return ('App\\Http\\Controllers\\' + this.outputFolder)
            .split(/[\\\/]/g)
            .filter(d => d)
            .join('\\')
    }
    get connectionStatement() {
        return this.connection != 'mysql' ? `protected $connection = '${this.connection}';` : ''
    }
    get primaryStatement() {
        return this.primaryKey != 'id' ? `protected $primaryKey = '${this.primaryKey}';` : ''
    }
    get primaryKey() {
        let lookForPrimary = this.fields.filter(f => f.key)
        let primaryExists = lookForPrimary.length > 0
        if (primaryExists) {
            return lookForPrimary[0].name
        } else {
            return '[][][unavailable][][]'
        }
    }
    get label() {
        return capitalize(separateWords(this.name))
    }
    /** @returns {function} */
    nameSpace(base) {
        return () => { }
    }
    toObject() {
        return {}
    }
}

/**
 * @typedef {object} FieldConstructor
 * @property {string} name
 * @property {string} type
 * @property {string} label
 * @property {string} columnType
 * @property {boolean} nullable
 * @property {string} [fieldType]
 * @property {boolean} [key]
 * @property {number} [length]
 * @property {string} [help]
 * @property {string} [comment]
 * @property {boolean} [heading]
 * @property {number} [baseMatrixSize]
 */

class Field {
    /**
     * 
     * @param {FieldConstructor} configurationObject 
     */
    constructor({
        name,
        type,
        fieldType = 'string',
        key = false,
        columnType,
        nullable = false,
        length = 191,
        label,
        help,
        comment,
        heading = false,
        baseMatrixSize = 2,
    }) {
        this.name = name
        this.type = type
        this.fieldType = fieldType
        this.key = key
        this.columnType = columnType
        this.nullable = nullable
        this.length = length
        this._label = label
        this.help = help
        this.comment = comment
        this.heading = heading
        this.baseMatrixSize = baseMatrixSize
    }
    get label() {
        return this._label
            ? this._label
            : this.name
    }
    methodName(prefix) {
        return prefix + capitalize(undasherize(this.name))
    }

}

export {
    Table,
    Field,
}