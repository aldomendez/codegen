import { writeFileSync, readFileSync, readdirSync } from 'fs'
import path from 'path'
import { createFolderInCaseOfError } from '../src/ensureFolderexists.js'

let __dirname = path.resolve(path.dirname(''))

function saveProject(project) {
    let directory = path.join(__dirname, 'projects', `${project.name}.${project.id}`)
    createFolderInCaseOfError(directory, () => {
        writeFileSync(path.join(directory, 'config.json'), JSON.stringify(project, null, 2))
    })
}

function readProject(project) {
    return readFileSync(path.join(__dirname, 'projects', project, 'config.json'),{encoding:'utf8', flag:'r'})
}

function directoryContent() {
    return readdirSync(path.resolve(__dirname, 'projects'))
}

export { saveProject, readProject, directoryContent }