import {existsSync, mkdirSync} from 'fs'

/**
 * 
 * @param {string} path
 * @returns {null}
 */
function ensureFolderExists(path){
    if(!existsSync(path)){
        mkdirSync(path, {recursive:true})
    }
}

/**
 * Tries to create a set of files into a folder, if failure occures try to create the folder and try again
 * @param {string} folder 
 * @param {function} fn A function that creates a set of files into the folder
 */
function createFolderInCaseOfError(folder, fn){
    try{
        fn()
    } catch (e){
        if(e.code === 'ENOENT'){
            ensureFolderExists(folder)
            fn()
        } else {
            throw new Error(e)
        }
    }
}
export {
    ensureFolderExists,
    createFolderInCaseOfError,
}