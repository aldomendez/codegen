import mariadb from 'mariadb'

const pool = null
// mariadb.createPool({
//   host: 'localhost',
//   user: 'root',
//   password: 'kerberos',
//   database: 'novali6_reloj',
//   connectionLimit: 5
// });

async function getDatabaseObjects() {
  return false;
  return pool.query(`SELECT
          columns.table_name table_name,
          columns.column_name column_name,
          columns.data_type data_type,
          columns.column_type column_type,
          columns.column_comment column_comment,
          columns.character_maximum_length character_maximum_length,
          columns.is_nullable is_nullable,
          columns.column_key column_key
        FROM
          information_schema.tables AS tables
          LEFT JOIN (
            SELECT
              *
            FROM
              information_schema.columns
            WHERE
              table_schema = DATABASE()
              AND data_type IN('char', 'varchar', 'text', 'mediumtext', 'enum', 'tinyint', 'int', 'bigint', 'float', 'double', 'date', 'time', 'datetime', 'timestamp', 'tinytext', 'decimal', 'numeric', 'smallint', 'mediumint')) AS columns ON (columns.table_name COLLATE utf8_bin = tables.table_name COLLATE utf8_bin)
        WHERE
          tables.table_schema = DATABASE()
        ORDER BY
      tables.table_name, columns.table_name COLLATE utf8_bin, columns.ordinal_position`)


}

export { getDatabaseObjects, pool }