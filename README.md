# CodeGen

La intension de esta herramienta es poder generar diferentes tipos de archivos que
generalmente se generan de manera manual o por medio de diferentes herramientas (muchas de ellas de pago).

Pero que son:
    - Basadas en la linea de comando, Y tienes que estar haciendo los archivos uno por uno y completando toda la logica interior conforme haga falta.
    - Sirven como punto de partida para que generes todas las partes de la estructura que hace falta, tiene algo de logica, pero solo genera los archivos de la parte del servidor y estan demasiado rebuscados como para poder generar otras plantillas
    - Se basan en sistemas de plantillas que son demaciado complejos.

Justo ahora esta herramienta es capaz de generar un archivo con la definicion de tablas, tal y como aparecen en la base de datos, y con esta informacion. Se alimenta a un script que se configura para ir generando las plantillas que hacen falta

Hasta el momento toda la logica esta explicita en los archivos que ejecutan los scripts. Pero a lo mejor en el futuro sea suficientemente robusto como para generar cosas por medio de una GUI que pueda hacer introspeccion directa con la base de datos.

Hasta ahorita lo que he generado son cosas sencillas:
    - Controladores de Laravel
    - Modelos de Laravel
    - Stores de Vuex
    - Scafolds de Cruds para usar en Vue

Lo que quiero lograr es hacer
    - DTO
    - Queries de configuracion para que la base de datos pueda llevar registro de cambios en las tablas criticas, por medio de triggers
    - Clases para usar con JavaScript y poder utilizar las funciones avanzadas de typescript (de preferencia utilizando JSDoc)
    - Migraciones
    - ViewModels
    - Relaciones entre tablas
    - Swagger API documentation
    - y muchas cosas en las que apenas comienzo a pensar

## Next steps

- Create a way to structure project data:
    - Target forlder
    - Target Database
- Maria DB integration
- Create a UI
- Create a standarized way to use the plugins, right now I use map and reduce. I want to use pipe
- Create a UI to manipulate the application data, with support to mutate the database


## Query to fetch all fields

```sql
SELECT
	tables.table_name,
	columns.table_name,
	columns.column_name,
	columns.data_type,
	columns.column_type,
	columns.column_comment,
	columns.character_maximum_length,
	columns.is_nullable,
	columns.column_key
FROM
	information_schema.tables AS tables
	LEFT JOIN (
		SELECT
			*
		FROM
			information_schema.columns
		WHERE
			table_schema = DATABASE()
			AND data_type IN('char', 'varchar', 'text', 'mediumtext', 'enum', 'tinyint', 'int', 'bigint', 'float', 'double', 'date', 'time', 'datetime', 'timestamp', 'tinytext', 'decimal', 'numeric', 'smallint', 'mediumint')) AS columns ON (columns.table_name COLLATE utf8_bin = tables.table_name COLLATE utf8_bin)
WHERE
	tables.table_schema = DATABASE()
ORDER BY
	tables.table_name, columns.table_name COLLATE utf8_bin, columns.ordinal_position
```


## Usos especificos de la aplicacion

### Validacion de la estructura de la base de datos contra los modelos declarados en la aplicacion

### Creacion de documentacion de la base de datos.
https://dataedo.com/docs

## Inspiracion
https://github.com/InfyOmLabs/laravel-generator

## MariaDB connector

[https://github.com/mariadb-corporation/mariadb-connector-nodejs](oficial mariadb connector repo)