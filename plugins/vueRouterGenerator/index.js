// @ts-check
import path from 'path'
import {ensureFolderExists, createFolderInCaseOfError} from '../../src/ensureFolderexists.js'
import template from './templates/vue/routes.js'
import { writeFileSync } from 'fs'

let __dirname = path.resolve(path.dirname(''))

/** @typedef {import('../../src/BaseObjects').Table} Table */

/**
 * 
 * @param {object} output 
 */
function excecute({output, filename, root}){
    
    /** @param {Table[]} table  */
    return (tables)=>{
        const template_dir = path.join(root, output)
        let fileContents = template(tables)
        
        createFolderInCaseOfError(template_dir, ()=>{
            writeFileSync(path.join(template_dir,  `${filename}.js`), fileContents)
        })
    
        console.log('Go to app/Providers/RouteServiceProvider.php')
        console.log('And register the new controller: homefully just add to the "map" function')
        console.log("Route::prefix('pix')->middleware('api')->group(base_path('routes/inmotion.php'));")

    }

}

export default excecute