//@ts-check
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table[]} tables  */
let template = (tables) => `const routes = [
${tables.map( t => `      {
    path: '/${t.name}',
    name: '${t.name}.index',
    component: () => import('@/cruds/${t.className}/Index.vue'),
    meta: { title: 'cruds.brand.title' }
  },
  {
    path: '/${t.name}/create',
    name: '${t.name}.create',
    component: () => import('@/cruds/${t.className}/Create.vue'),
    meta: { title: 'cruds.brand.title' }
  },
  {
    path: '/${t.name}/:id',
    name: '${t.name}.show',
    component: () => import('@/cruds/${t.className}/Show.vue'),
    meta: { title: 'cruds.brand.title' }
  },
  {
    path: '/${t.name}/:id/edit',
    name: '${t.name}.edit',
    component: () => import('@/cruds/${t.className}/Edit.vue'),
    meta: { title: 'cruds.brand.title' }
  },
  
` ).join('\n')}
]

export {routes}
`

export default template