//@ts-check
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) =>  `
import {cloneDeep} from 'lodash'
import axios from 'axios'

const set = key => (state, val) => {
    state[key] = val
  }
  
  function getDefaultState() {
    return {
      data: [],
      total: 0,
      query: {},
      loading: false
    }
  }
  
  const getters = {
    data: state => state.data,
    total: state => state.total,
    loading: state => state.loading
  }
  
  const actions = {
    fetchIndexData({ commit, state }) {
      commit('setLoading', true)
      axios
        .get('pix/v2/${table.name}', { params: state.query })
        .then(response => {
          commit('setData', response.data.data)
          commit('setTotal', response.data.total)
        })
        .catch(error => {
          message = error.response.data.message || error.message
          // TODO error handling
        })
        .finally(() => {
          commit('setLoading', false)
        })
    },
    destroyData({ commit, state, dispatch }, id) {
      axios
        .delete('pix/v2/${table.name}/' + id)
        .then(response => {
          dispatch('fetchIndexData')
        })
        .catch(error => {
          message = error.response.data.message || error.message
          // TODO error handling
        })
    },
    setQuery({ commit }, value) {
      commit('setQuery', cloneDeep(value))
    },
    resetState({ commit }) {
      commit('resetState')
    }
  }
  
  const mutations = {
    setData: set('data'),
    setTotal: set('total'),
    setQuery: set('query'),
    setLoading: set('loading'),
    resetState(state) {
      Object.assign(state, getDefaultState())
    }
  }
  
  export default {
    namespaced: true,
    state: getDefaultState,
    getters,
    actions,
    mutations
  }
  `
  // ${table.fields.map(d => `${d.setMethodName}: set('${d.name}'),`).join('\n    ')}
  export default template