//@ts-check
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) =>  `<template>
<div class="space-y-6 sm:px-6 lg:px-0 lg:col-span-9">
<section aria-labelledby="payment_details_heading">
  <form action="#" method="POST">
    <div class="shadow sm:rounded-md sm:overflow-hidden">
      <div class="bg-white py-6 px-4 sm:p-6">
        <div>
          <h2 id="payment_details_heading" class="text-lg leading-6 font-medium text-gray-900">${table.label}</h2>
          <!-- <p class="mt-1 text-sm text-gray-500">Update your billing information. Please note that updating your location could affect your tax rates.</p> -->
        </div>
        
        <div class="mt-6 grid grid-cols-4 gap-6">
        ${table.fields.map( f => `<div class="col-span-4 sm:col-span-2">
        <label for="${f.name}" class="block text-sm font-medium text-gray-700">${f.label}</label>
        <input v-model="entry.${f.name}" type="text" name="${f.name}" id="${f.name}" class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm">
      </div>`).join('\n')}

          
        </div>
      </div>
      <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
        <button type="submit" class="bg-gray-800 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-900">
          Save
        </button>
      </div>
    </div>
  </form>
</section>
</div>
</template>

<script>
import { mapGetters, mapActions } from 'vuex'

import store from '@/store'
import ${table.name} from '@/stores/${table.className}/${table.name}_single.js';
if (!store.state['${table.className}Single']) {
  store.registerModule('${table.className}Single', ${table.name});
}


export default {
  name:'${table.className}Show',
components: {
},
data() {
  return {}
},
beforeDestroy() {
  this.resetState()
},
computed: {
  ...mapGetters('${table.className}Single', ['entry'])
},
watch: {
  '$route.params.id': {
    immediate: true,
    handler() {
      this.resetState()
      this.fetchShowData(this.$route.params.id)
    }
  }
},
methods: {
  ...mapActions('${table.className}Single', ['fetchShowData', 'resetState'])
}
}
</script>
`
  export default template