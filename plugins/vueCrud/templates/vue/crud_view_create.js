//@ts-check
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) =>  `<template>
<div class="container-fluid">
  <form @submit.prevent="submitForm">
    ${table.fields.map( f => `<div>
    <label for="email" class="block text-sm font-medium text-gray-700">${f.name}</label>
    <div class="mt-1">
      <input type="text" name="email" id="email" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="you@example.com">
    </div>
  </div>`).join('\n')}
    
  </form>
</div>
</template>

<script>
import { mapGetters, mapActions } from 'vuex'
import {delay} from 'lodash'
export default {
data() {
  return {
    status: '',
    activeField: ''
  }
},
computed: {
  ...mapGetters('InventoriesSingle', ['entry', 'loading', 'lists'])
},
mounted() {
  this.fetchCreateData()
},
beforeDestroy() {
  this.resetState()
},
methods: {
  ...mapActions('InventoriesSingle', [
    ${table.fields.map(d => `'${d.methodName('set')}',`).join('\n    ')}
    // 'setColor',
    // 'setQuantity',
    // 'setModel',
    'storeData',
    'resetState',
    'fetchCreateData',
  ]),
  ${table.fields.map(d => `${d.methodName('update')}(e){
    this.${d.methodName('set')}(e.target.value)
  },`).join('\n  ')}
  // updateColor(e) {
  //   this.setColor(e.target.value)
  // },
  // updateQuantity(e) {
  //   this.setQuantity(e.target.value)
  // },
  // updateModel(value) {
  //   this.setModel(value)
  // },
  submitForm() {
    this.storeData()
      .then(() => {
        this.$router.push({ name: 'inventories.index' })
        this.$eventHub.$emit('create-success')
      })
      .catch(() => {
        this.status = 'failed'
        delay(() => {
          this.status = ''
        }, 3000)
      })
  },
  focusField(name) {
    this.activeField = name
  },
  clearFocus() {
    this.activeField = ''
  }
}
}
</script>
`
  export default template