//@ts-check
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) =>  `<template>
<div class="lg:grid lg:grid-cols-12 lg:gap-x-5">
  <div class="space-y-4 py-2 sm:px-4 lg:px-0 lg:col-span-12">
    <!-- <Actions :options="options" @refresh="resetState"/> -->
    <section aria-labelledby="current_inventory">
      <div
        class="bg-white pt-4 space-y-4 shadow sm:rounded-md sm:overflow-hidden"
      >
        <div class="px-4 sm:px-6 flex justify-between items-center">
          <h2
            id="current_inventory"
            class="text-lg leading-6 font-medium text-gray-900"
          >
          ${table.label}
          </h2>
          <div class="max-w-xs w-full">
            <label for="search" class="sr-only">Search</label>
            <div class="relative">
              <div
                class="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center"
              >
                <Icon
                  name="search"
                  class="flex-shrink-0 h-5 w-5 text-gray-400"
                />
              </div>
              <input
                v-model="search"
                type="search"
                class="block w-full bg-white border border-gray-300 rounded-md py-2 pl-10 pr-3 text-sm leading-5 placeholder-gray-500 focus:outline-none focus:text-gray-900 focus:placeholder-gray-400 focus:border-blue-300 focus:shadow-outline-blue sm:text-sm transition duration-150 ease-in-out"
                placeholder="Search"
              />
            </div>
          </div>
        </div>
        <Table :fields="tableFields" :data="data">
          <template #review="{ ${table.name} }">
            <button
              class="flex text-blue-600 hover:text-blue-900"
            >
              <Icon name="clipboard-list" class="h-5 w-5 mr-1" />
              Movements</button>
          </template>
          <template #delete="{ ${table.name}, row }">
            <button
              type="button"
              class="flex text-red-600 hover:text-red-800"
            >
              <Icon name="minus-circle" class="h-5 w-5 mr-1" />Remove
            </button>
          </template>
        </Table>
      </div>
    </section>
  </div>
</div>
</template>

<script>
import { mapGetters, mapActions } from 'vuex'
import Table from '@/components/Table2/Table.vue'
import Icon from '@/components/Icon.vue'
// import Actions from './CurrentInventory/Actions.vue'

import store from '@/store'
import ${table.name} from '@/stores/${table.className}/${table.name}_multi.js';
if (!store.state['${table.className}Index']) {
  store.registerModule('${table.className}Index', ${table.name});
}

export default {
components: {
  Table,
  Icon,
  // Actions,
},
data() {
  return {
    options: [{ title: 'Add' }],
    search:'',
    tableFields: [
      ${table.fields.map( f => `{ label: '${f.label}', field: '${f.name}', key: ${f.key} },`).join('\n        ')}
      { screenReader: 'View movements', slot: 'review' },
      { screenReader: 'View movements', slot: 'delete' },
    ],
    query: { sort: '${table.primaryKey}', order: 'desc', limit: 100, offset: 0 },
    xprops: {
      module: '${table.className}Index',
      route: 'inventories',
      permission_prefix: 'inventory_'
    }
  }
},
beforeDestroy() {
  this.resetState()
},
computed: {
  ...mapGetters('${table.className}Index', ['data', 'total', 'loading'])
},
watch: {
  query: {
    immediate: true,
    handler(query) {
      query.page = (query.offset + query.limit) / query.limit
      this.setQuery(query)
      this.fetchIndexData()
    },
    deep: true
  }
},
methods: {
  ...mapActions('${table.className}Index', [
    'fetchIndexData',
    'setQuery',
    'resetState'
  ])
}
}
</script>
`
  export default template