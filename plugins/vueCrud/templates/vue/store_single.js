//@ts-check
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) =>  `import axios from 'axios'
function initialState() {
  return {
    entry: {
      ${table.fields.map( d => `${d.name}:'',`).join('\n      ')}
    },
    lists: {
      model: []
    },
    loading: false
  }
}

const route = 'pix/v2/${table.name}'

const getters = {
  entry: state => state.entry,
  lists: state => state.lists,
  loading: state => state.loading
}

const actions = {
  storeData({ commit, state, dispatch }) {
    commit('setLoading', true)
    dispatch('Alert/resetState', null, { root: true })

    return new Promise((resolve, reject) => {
      let params = objectToFormData(state.entry, {
        indices: true,
        booleansAsIntegers: true
      })
      axios
        .post(route, params)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          let message = error.response.data.message || error.message
          let errors = error.response.data.errors

          dispatch(
            'Alert/setAlert',
            { message: message, errors: errors, color: 'danger' },
            { root: true }
          )

          reject(error)
        })
        .finally(() => {
          commit('setLoading', false)
        })
    })
  },
  updateData({ commit, state, dispatch }) {
    commit('setLoading', true)
    dispatch('Alert/resetState', null, { root: true })

    return new Promise((resolve, reject) => {
      let params = objectToFormData(state.entry, {
        indices: true,
        booleansAsIntegers: true
      })
      params.set('_method', 'PUT')
      axios
        .post(\`\${route}/\${state.entry.${table.primaryKey}}\`, params)
        .then(response => {
          commit('setEntry', response.data)
          resolve(response)
        })
        .catch(error => {
          let message = error.response.data.message || error.message
          let errors = error.response.data.errors

          dispatch(
            'Alert/setAlert',
            { message: message, errors: errors, color: 'danger' },
            { root: true }
          )

          reject(error)
        })
        .finally(() => {
          commit('setLoading', false)
        })
    })
  },
  
  ${table.fields.map( d => `${d.methodName('set')}({ commit }, value) {
    commit('${d.name}', value)
  },`).join('\n  ')}

  fetchCreateData({ commit }) {
    axios.get(\`\${route}/create\`).then(response => {
      commit('setLists', response.data.meta)
    })
  },
  fetchEditData({ commit, dispatch }, id) {
    axios.get(\`\${route}/\${id}/edit\`).then(response => {
      commit('setEntry', response.data)
      commit('setLists', response.data.meta)
    })
  },
  fetchShowData({ commit, dispatch }, id) {
    axios.get(\`\${route}/\${id}\`).then(response => {
      commit('setEntry', response.data)
    })
  },
  resetState({ commit }) {
    commit('resetState')
  }
}

const mutations = {
  ${table.fields.map( d => `${d.methodName('set')}(state, value) {
    state.${d.name} = value
  },`).join('\n  ')}

  setEntry(state, entry) {
    state.entry = entry
  },
  setLists(state, lists) {
    state.lists = lists
  },
  setLoading(state, loading) {
    state.loading = loading
  },
  resetState(state) {
    state = Object.assign(state, initialState())
  }
}

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
}
`
  export default template