// @ts-check
import path from 'path'
import { ensureFolderExists, createFolderInCaseOfError } from '../../src/ensureFolderexists.js'
import singleTemplate from './templates/vue/store_single.js'
import multiTemplate from './templates/vue/store_multi.js'
import vueCreateTemplate from './templates/vue/crud_view_create.js'
import vueEditTemplate from './templates/vue/crud_view_edit.js'
import vueIndexTemplate from './templates/vue/crud_view_index.js'
import vueShowTemplate from './templates/vue/crud_view_show.js'
import { writeFileSync } from 'fs'

let __dirname = path.resolve(path.dirname(''))

/** @typedef {import('../../src/BaseObjects').Table} Table */

function excecute({
    root
}) {

    /** @param {Table} table  */
    return (table) => {
        const storesDir = path.join(root, 'src/stores/', table.className)
        console.log(storesDir)
        let singleContents = singleTemplate(table)
        let multiContents = multiTemplate(table)

        createFolderInCaseOfError(storesDir, () => {
            writeFileSync(path.join(storesDir, table.name + '_single.js'), singleContents)
            writeFileSync(path.join(storesDir, table.name + '_multi.js'), multiContents)
        })

        const viewsDir = path.join(root, '/src/cruds/', table.className)
        
        let vueCreateContents = vueCreateTemplate(table)
        let vueEditContents = vueEditTemplate(table)
        let vueIndexContents = vueIndexTemplate(table)
        let vueShowContents = vueShowTemplate(table)

        createFolderInCaseOfError(viewsDir, () => {
            writeFileSync(path.join(viewsDir, 'Create.vue'), vueCreateContents)
            writeFileSync(path.join(viewsDir, 'Edit.vue'), vueEditContents)
            writeFileSync(path.join(viewsDir, 'Index.vue'), vueIndexContents)
            writeFileSync(path.join(viewsDir, 'Show.vue'), vueShowContents)
        })

        return table
    }
}

export default excecute