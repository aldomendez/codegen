//@ts-check
/** @typedef {import('../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) => `<?php

namespace ${table.controllerNameSpace};

use Gate;
${table.classUseStatement}
use Illuminate\\Http\\Request;
use Illuminate\\Http\\Response;
use App\\Http\\Controllers\\Controller;

class ${table.className}Controller extends Controller
{
    public function index(Request $request)
    {
        $query = ${table.className}::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        \$${table.name} = $query->get();

        return [
            'data' => \$${table.name},
            'total' => 100,
        ];
    }

    public function store(Request $request)
    {
        $request->validate([
            ${table.validations.map(d => `'${d.name}' => 'required',`).join('\n            ')}
        ]);

        \$${table.name} = ${table.className}::create(
            $request->only([${table.fields.map(d => `'${d.name}',`).join('')}])
        );

        return \$${table.name};
    }

    /**
     * This method return the associated data required to create a new ${table.className}
     */
    public function create(ContactContact $contactContact)
    {
        return response([
            'meta' => [
                // use this place to populate any related information to allow proper 
                // form fill
                // 'company' => ContactCompany::get(['id', 'company_name']),
            ],
        ]);
    }

    public function show(${table.className} \$${table.name})
    {
        // abort_if(Gate::denies('contact_contact_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if (empty(\$${table.name})) {
            return $this->sendError('${table.name} not found');
        }

        return \$${table.name};
    }

    public function update(Request $request, ${table.className} \$${table.name})
    {
        $request->validate([
            ${table.validations.map(d => `'${d.name}' => 'required',`).join('\n            ')}
        ]);
        
        \$${table.name}->update($request->validated());

        return \$${table.name};
    }

    public function destroy(${table.className} \$${table.name})
    {
        if (empty(\$${table.name})) {
            return $this->sendError('${table.name} not found');
        }
        \$${table.name}->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

`

export default template