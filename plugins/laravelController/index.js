// @ts-check
import path from 'path'
import {ensureFolderExists, createFolderInCaseOfError} from '../../src/ensureFolderexists.js'
import template from './templates/controller.js'
import { writeFileSync } from 'fs'

let __dirname = path.resolve(path.dirname(''))

var compiledTemplate = template

/** @typedef {import('../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
function excecute(table) {

    const template_dir = path.join(__dirname, '..', 'app/Http/Controllers/', table.outputFolder,)
    
    let fileContents = compiledTemplate(table)

    createFolderInCaseOfError(template_dir, ()=>{
        writeFileSync(path.join(template_dir, table.fileName + 'Controller.php'), fileContents)
    })

    return table
}

export default excecute