// @ts-check
import path from 'path'
import {ensureFolderExists, createFolderInCaseOfError} from '../../src/ensureFolderexists.js'
import template from './templates/routes.js'
import { writeFileSync } from 'fs'

let __dirname = path.resolve(path.dirname(''))


/** @typedef {import('../../src/BaseObjects').Table} Table */
/** @param {Table[]} table  */
function excecute(table, filename = 'inmotion'){

    const template_dir = path.join(__dirname, '..', 'routes')
    let fileContents = template(table)
    
    createFolderInCaseOfError(template_dir, ()=>{
        writeFileSync(path.join(template_dir, filename + '.php'), fileContents)
    })

    console.log('Go to app/Providers/RouteServiceProvider.php')
    console.log('And register the new controller: homefully just add to the "map" function')
    console.log("Route::prefix('pix')->middleware('api')->group(base_path('routes/inmotion.php'));")

}

export default excecute