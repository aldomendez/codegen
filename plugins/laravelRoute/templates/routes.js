//@ts-check
/** @typedef {import('../../../src/BaseObjects').Table} Table */
/** @param {Table[]} tables  */
let template = (tables) => `<?php
${tables.map( t => `use ${t.controllerNameSpace}\\${t.className}Controller;` ).join('\n')}

Route::group([
    'prefix' => 'v2', 
    'middleware' => []
], function () {
    
${tables.map( t => `    //${t.className}
    Route::post('${t.name}', [${t.className}Controller::class, 'store']);
    Route::get('${t.name}', [${t.className}Controller::class, 'index']);
    Route::delete('${t.name}/{${t.name}}', [${t.className}Controller::class, 'destroy']);
    Route::patch('${t.name}/{${t.name}}', [${t.className}Controller::class, 'update']);
    Route::get('${t.name}/{${t.name}}/edit', [${t.className}Controller::class, 'show']);\n` ).join('\n')}
});
`

export default template