// @ts-check
import path from 'path'
import { createFolderInCaseOfError } from '../../src/ensureFolderexists.js'
import { writeFileSync } from 'fs'

import singleTemplate from './templates/vue/store.js'
import createTemplate from './templates/vue/crud_view_create.js'
import editTemplate from './templates/vue/crud_view_edit.js'
import indexTemplate from './templates/vue/crud_view_index.js'


let __dirname = path.resolve(path.dirname(''))

/** @typedef {import('../../src/BaseObjects').Table} Table */
function excecute({
    root
}) {

    /** @param {Table} table  */
    return (table) => {

        try {

            const storesDir = path.join(root, 'src/stores/')
            // console.log(storesDir)

            createFolderInCaseOfError(storesDir, () => {
                writeFileSync(path.join(storesDir, table.name + '.js'), singleTemplate(table))
            })

            const viewsDir = path.join(root, '/src/views/', table.className)

            createFolderInCaseOfError(viewsDir, () => {
                writeFileSync(path.join(viewsDir, 'Create.vue'), createTemplate(table))
                writeFileSync(path.join(viewsDir, 'Edit.vue'), editTemplate(table))
                writeFileSync(path.join(viewsDir, 'Index.vue'), indexTemplate(table))
            })
        } catch (error) {
            console.error(error)
        }

        return table
    }
}

export default excecute