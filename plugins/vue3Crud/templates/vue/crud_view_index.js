//@ts-check
import {replace,snakeCase, upperFirst} from 'lodash-es'
const propper = (t) => replace(upperFirst(snakeCase(t)), /_/g, ' ')
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) =>  `<template><GuardGroupView
:groups="[
  'pix.admin',
  'truck_tracker.admin',
]"
>
<ShellHeader @search="()=>getList({page:a,vendor:$route.query.vendor})"></ShellHeader>
<main class="flex-1 relative pb-8 z-0 overflow-y-auto">
  <!-- Page header -->
  <div class="bg-white shadow">
    <div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
      <div class="py-6 sm:flex sm:items-center sm:justify-between lg:border-t lg:border-gray-200">
        <div class="flex-1 min-w-0">
          <!-- Profile -->
          <div class="flex items-center">
            <!-- <img class="hidden h-16 w-16 rounded-full sm:block" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixqx=676cMkIELI&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.6&w=256&h=256&q=80" alt=""> -->
            <div>
              <div class="flex items-center">
                <!-- <img class="h-16 w-16 rounded-full sm:hidden" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixqx=676cMkIELI&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.6&w=256&h=256&q=80" alt=""> -->
                <h1
                  class="ml-3 text-2xl font-bold leading-7 text-gray-900 sm:leading-9 sm:truncate"
                >${propper(table.name)}</h1>
              </div>
            </div>
          </div>
        </div>
        <div class="mt-6 flex space-x-3 md:mt-0 md:ml-4">
          <a
            v-if="$route.query.vendor"
            :href="\`#/\${model.path}\`"
            class="inline-flex items-center px-4 py-1 border border-gray-300 shadow-sm text-sm font-medium rounded-sm text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
          >All</a>
          <a
            :href="\`#/\${model.path}/new\`"
            class="inline-flex items-center px-4 py-1 border border-transparent shadow-sm text-sm font-medium rounded-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
          >Create</a>
        </div>
      </div>
    </div>
  </div>

  <div class="mt-8">
    <div v-if="false" class="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
      <h2 class="text-lg leading-6 font-medium text-gray-900">Overview</h2>
      <div class="mt-2 grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-3">
        <!-- Card -->

        <div class="bg-white overflow-hidden shadow rounded-sm">
          <div class="p-5">
            <div class="flex items-center">
              <div class="flex-shrink-0">
                <!-- Heroicon name: outline/scale -->
                <svg
                  class="h-6 w-6 text-gray-400"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  aria-hidden="true"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M3 6l3 1m0 0l-3 9a5.002 5.002 0 006.001 0M6 7l3 9M6 7l6-2m6 2l3-1m-3 1l-3 9a5.002 5.002 0 006.001 0M18 7l3 9m-3-9l-6-2m0-2v2m0 16V5m0 16H9m3 0h3"
                  />
                </svg>
              </div>
              <div class="ml-5 w-0 flex-1">
                <dl>
                  <dt class="text-sm font-medium text-gray-500 truncate">Account balance</dt>
                  <dd>
                    <div class="text-lg font-medium text-gray-900">$30,659.45</div>
                  </dd>
                </dl>
              </div>
            </div>
          </div>
          <div class="bg-gray-50 px-5 py-3">
            <div class="text-sm">
              <a href="#" class="font-medium text-blue-700 hover:text-blue-900">View all</a>
            </div>
          </div>
        </div>

        <!-- More items... -->
      </div>
    </div>

    <h2
      class="max-w-6xl mx-auto mt-8 px-4 text-lg leading-6 font-medium text-gray-900 sm:px-6 lg:px-8"
    >Available</h2>

    <!-- <pre class="text-xs">{{model.list}}</pre> -->

    <!-- Activity list (smallest breakpoint only) -->
    <!-- TODO Populate this template with the current information of the list -->
    <div v-if="false" class="shadow sm:hidden">
      <ul class="mt-2 divide-y divide-gray-200 overflow-hidden shadow sm:hidden">
        <li v-for="(v) in model.list" :key="v.id">
          <a :href="\`#/model/\${v.id}\`" class="block px-4 py-4 bg-white hover:bg-gray-50">
            <span class="flex items-center space-x-4">
              <span class="flex-1 flex space-x-2 truncate">
                <Icon
                  :icon="icons.cube"
                  class="flex-shrink-0 h-5 w-5 text-gray-400 mt-1"
                />
                <span class="flex flex-col text-gray-500 text-sm truncate">
                  <span class="truncate text-gray-700 font-semibold">{{v.part}}</span>
                  <span v-if="v.vendor.name" class="flex">
                    <Icon
                      :icon="icons.officeBuilding"
                      class="flex-shrink-0 h-4 w-4 text-gray-500 mt-1"
                    />
                    {{v.vendor.name}}
                  </span>
                  <span v-if="v.description" class="flex">
                    {{v.description}}
                  </span>
                  <span v-if="v.type" class="flex">
                    <span>type: </span>
                    {{v.type}}
                  </span>
                  <span v-if="v.hts" class="flex">
                    <span>hts: </span>
                    {{v.hts}}
                  </span>
                  <span v-if="v.pricex" class="flex">
                    <span>pricex: </span>
                    {{v.pricex}}
                  </span>
                </span>
              </span>
              <!-- Heroicon name: solid/chevron-right -->
              <svg
                class="flex-shrink-0 h-5 w-5 text-gray-400"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fill-rule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clip-rule="evenodd"
                />
              </svg>
            </span>
          </a>
        </li>

        <!-- More transactions... -->
      </ul>

      <Pagination :data="model.pagination" @next="(a)=>getList(a)" @prev="(a)=>getList(a)"></Pagination>
    </div>
    <!-- Activity table (small breakpoint and up) -->

    <Table :fields="tableFields" :data="Object.values(model.list)" class="hidden sm:block">
      <template #actions="{dat}">
        <div class="flex space-x-1 justify-end">
          <!-- <a :href="\`#/${table.name}?vendor=\${dat.id}\`" :class="styles.button">
            <Icon :icon="icons.briefcase" class="w-4 h-4" />
          </a>
          <a :href="\`#/${table.name}?vendor=\${dat.id}\`" :class="styles.button">
            <Icon :icon="icons.cube" class="w-4 h-4" />
          </a> -->
          <a :href="\`#/\${model.path}/\${dat.id}\`" :class="styles.button">
            <Icon :icon="icons.pencil" class="w-4 h-4" />
          </a>
        </div>
      </template>
      <template #pagination>
        <Pagination :data="model.pagination" @next="(a)=>getList({page:a,vendor:$route.query.vendor})" @prev="(a)=>getList({page:a,vendor:$route.query.vendor})"></Pagination>
      </template>
    </Table>
  </div>
</main>
</GuardGroupView>
</template>
<script>
import { model, getList } from "../../stores/${table.name}";
import Table from "../components/Table";
import Pagination from "../components/Pagination";
import Icon, { briefcase, cube, pencil, officeBuilding } from "../components/Icon";
import ShellHeader from "../UIShellHeader.vue";

export default {
components: {
  Table,
  Icon,
  Pagination,
  ShellHeader,
},
setup() {
  const tableFields = [
    ${table.fields.filter(d => d.inIndex).map( d => `{ label: '${propper(d.name)}', field: '${d.name}'},`).join('\n    ')}
    { screenReader: "Actions", slot: "actions" },
  ];
  const icons = {
    briefcase,
    pencil,
    officeBuilding,
    cube,
  };
  const styles = {
    button:
      "relative inline-flex items-center px-2 py-1 border border-gray-300 text-sm font-medium rounded-sm text-gray-700 bg-white hover:bg-gray-50",
  };
  const search = (a) => console.log(a);
  return {
    styles,

    search,
    icons,
    tableFields,
    model,
    getList,
  };
},
created() {
  this.$watch(
    () => this.$route.query,
    (toParams, preParams) => {
      let params = {}
      console.log(toParams);
      // getInstance(toParams.signage_id);
      if(toParams.hasOwnProperty('vendor')){
        params.vendor = toParams.vendor
      }
      this.getList(params);
    },
    { immediate: true }
  );
},
};
</script>
`
  export default template