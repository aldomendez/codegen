//@ts-check
import {replace,snakeCase, upperFirst} from 'lodash-es'
const propper = (t) => replace(upperFirst(snakeCase(t)), /_/g, ' ')
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) =>  `<template><GuardGroupView
:groups="[
  'pix.admin',
  'truck_tracker.admin',
]"
>
<ShellHeader @search="(q) => search(q)"></ShellHeader>
<main class="flex-1 relative pb-8 z-0 overflow-y-auto">
  <!-- Page header -->
  <div class="bg-white shadow">
    <div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
      <div class="py-6 md:flex md:items-center md:justify-between lg:border-t lg:border-gray-200">
        <div class="flex-1 min-w-0">
          <!-- Profile -->
          <div class="flex items-center">
            <div>
              <div class="flex items-center">
                <h1
                  class="ml-3 text-2xl font-bold leading-7 text-gray-900 sm:leading-9 sm:truncate"
                >${propper(table.name)}</h1>
              </div>
              <dl
                v-if="false"
                class="mt-6 flex flex-col sm:ml-3 sm:mt-1 sm:flex-row sm:flex-wrap"
              >
                <dt class="sr-only">${propper(table.name)}</dt>
                <dd
                  class="flex items-center text-sm text-gray-500 font-medium capitalize sm:mr-6"
                >
                  <!-- Heroicon name: solid/office-building -->
                  <svg
                    class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                    aria-hidden="true"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M4 4a2 2 0 012-2h8a2 2 0 012 2v12a1 1 0 110 2h-3a1 1 0 01-1-1v-2a1 1 0 00-1-1H9a1 1 0 00-1 1v2a1 1 0 01-1 1H4a1 1 0 110-2V4zm3 1h2v2H7V5zm2 4H7v2h2V9zm2-4h2v2h-2V5zm2 4h-2v2h2V9z"
                      clip-rule="evenodd"
                    />
                  </svg>
                  Duke street studio
                </dd>
                <dt class="sr-only">Account status</dt>
                <dd
                  class="mt-3 flex items-center text-sm text-gray-500 font-medium sm:mr-6 sm:mt-0 capitalize"
                >
                  <!-- Heroicon name: solid/check-circle -->
                  <svg
                    class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                    aria-hidden="true"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                      clip-rule="evenodd"
                    />
                  </svg>
                  Verified account
                </dd>
              </dl>
            </div>
          </div>
        </div>
        <div class="mt-6 flex space-x-3 md:mt-0 md:ml-4">
          <button
            type="button"
            @click="show($route.params.id)"
            class="inline-flex items-center px-4 py-1 border border-gray-300 shadow-sm text-sm font-medium rounded-sm text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
          >Refresh</button>
          <button
            @click="$router.back"
            type="button"
            class="inline-flex items-center px-4 py-1 border border-transparent shadow-sm text-sm font-medium rounded-sm text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500"
          >Cancel</button>
        </div>
      </div>
    </div>
  </div>

  <div>
    <!-- <h2
      class="max-w-6xl mx-auto mt-8 px-4 text-lg leading-6 font-medium text-gray-900 sm:px-6 lg:px-8"
    >New</h2>-->

    <div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
      <!-- <pre class="text-xs">{{model.fields}}</pre> -->
      <Form :schema="schema" :data="model.fields" :errors="model.errors">
        <template #buttons>
          <div class="pt-5">
            <div class="flex justify-between">
              <div class="flex justify-start">
                <button
                  @click="destroy"
                  type="button"
                  class="bg-white py-2 px-4 border border-red-300 rounded-sm shadow-sm text-sm font-medium text-red-700 hover:bg-red-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
                >Destroy</button>
              </div>
              <div class="flex justify-end">
                <button
                  @click="$router.back"
                  type="button"
                  class="bg-white py-2 px-4 border border-gray-300 rounded-sm shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                >Cancel</button>
                <button
                  type="button"
                  @click="update"
                  class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                >Update</button>
              </div>
            </div>
          </div>
        </template>
      </Form>
    </div>

    <!-- <pre class="text-xs">{{model.fields}}</pre> -->

    <!-- Activity list (smallest breakpoint only) -->
  </div>
</main>
</GuardGroupView>
</template>
<script>
import {
model,
show,
schema,
update,
destroy,
} from "../../stores/${table.name}";
import Icon, { briefcase, cube } from "../components/Icon";
import { Form } from "../components/Form";
import ShellHeader from "../UIShellHeader.vue";

export default {
components: {
  Icon,
  ShellHeader,
  Form,
},
setup() {
  const icons = {
    briefcase,
    cube,
  };
  return {
    icons,
    model,
    show,
    destroy,
    schema,
    update,
  };
},
created() {
  this.$watch(
    () => this.$route.params,
    (toParams, preParams) => {
      // console.log(toParams);
      // getInstance(toParams.signage_id);
      if (toParams.hasOwnProperty("id")) {
        show(toParams.id);
      }
    },
    { immediate: true }
  );
},
};
</script>
`
  export default template