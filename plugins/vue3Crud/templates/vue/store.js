//@ts-check
import {replace,snakeCase, upperFirst} from 'lodash-es'
/** @typedef {import('../../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) =>  `import { reactive, ref } from 'vue'
import { get, post, put, del } from '../libs/fetch'
import { search } from './shell'
import { loading } from './services'

const model = reactive({
  name: '${replace(upperFirst(snakeCase(table.name)), /_/g, ' ')}',
  path: '${table.name}',
  list: {},
  fields: {
    ${table.fields.map( d => `${d.name}:'',`).join('\n    ')}
  },
  errors: {},
  pagination: {
    currentPage: 0,
    from: 0,
    lastPage: 0,
    perPage: 0,
    to: 0,
    total: 0,
  }
})
const api = 'api/shipment/${table.name}'

const clearList = () => {
  model.list = []
}

const emptyFields = () => {
  model.fields = {
    ${table.fields.map( d => `${d.name}:'',`).join('\n    ')}
  }
}
const getList = (page = 1) => {
  loading.value = true
  return get(\`\${api}\`, {
    params: {
      page,
      search: search.value,
    }
  }).then(inter => {
    // console.log(inter)
    let content = inter.data
    model.list = content

    // model.pagination.currentPage = content.current_page
    // model.pagination.from = content.from
    // model.pagination.lastPage = content.last_page
    // model.pagination.perPage = content.per_page
    // model.pagination.to = content.to
    // model.pagination.total = content.total

  }).catch(err => {
    if (err.response.errors) {
      model.errors = err.response.errors
    }
  }).finally(() => {
    loading.value = false
  })
}
const create = () => {
  loading.value = true
  model.errors = {}
  return post(\`\${api}\`, model.fields)
    .then(inter => {
      emptyFields()
    }).catch(err => {
      // console.log(err.response)
      if (err.response.data.errors) {
        model.errors = err.response.data.errors
      }
    }).finally(() => {
      loading.value = false
    })

}
const show = (id) => {
  loading.value = true
  model.errors = {}
  return get(\`\${api}/\${id}\`, model.fields)
    .then(inter => {
      model.fields = inter.data

    }).catch(err => {
      // console.log(err.response)
      if (err.response.data.errors) {
        model.errors = err.response.data.errors
      }
    }).finally(() => {
      loading.value = false
    })

}
const update = () => {
  loading.value = true
  model.errors = {}
  return put(\`\${api}/\${model.fields.id}\`, model.fields)
    .then(inter => {
      let content = inter.data
      // console.log(inter.data)
      model.fields = inter.data
    }).catch(err => {
      // console.log(err.response)
      if (err.response.data.errors) {
        model.errors = err.response.data.errors
      }
    }).finally(() => {
      loading.value = false
    })

}

const destroy = () => {
  loading.value = true
  model.errors = {}
  return del(\`\${api}/\${model.fields.id}\`)
    .then(inter => {
      let content = inter.data
      // console.log(inter.data)
      model.fields = inter.data
    }).catch(err => {
      // console.log(err.response)
      if (err.response.data.errors) {
        model.errors = err.response.data.errors
      }
    }).finally(() => {
      loading.value = false
    })

}

const schema = [
  {
    type: "group",
    attributes: {
      title: "Basic vendor information",
      fields: [
        ${table.fields.filter(d=>d.inIndex).map( d => `{
          type: "text",
          attributes: {
            id: "${d.name}",
            class: 'sm:col-span-3',
            label: "${replace(upperFirst(snakeCase(d.name)), /_/g, ' ')}",
          },
        },`).join('\n      ')}
      ],
    },
  },
];

export {
  model,
  clearList,
  getList,
  schema,
  create,
  destroy,
  show,
  update,
  emptyFields,
}
`
  export default template