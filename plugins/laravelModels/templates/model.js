//@ts-check
/** @typedef {import('../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) => `<?php

namespace ${table.classNameSpace};

// use App\\Support\\HasAdvancedFilter;
// use Illuminate\\Database\\Eloquent\\Factories\\HasFactory;
use Illuminate\\Database\\Eloquent\\Model;
use \\DateTimeInterface;
${table.hasSoftDeletes?'use Illuminate\\Database\\Eloquent\\SoftDeletes;':''}

class ${table.className} extends Model
{
    // use ${table.hasSoftDeletes?'SoftDeletes, ':''}HasFactory;
    ${table.connectionStatement}
    ${table.primaryStatement}
    public $table = '${table.table}';
    ${table.hasDateFields? `
    protected $dates = [
        ${table.dateFields.map(d=>`'${d.name}',`).join('\n        ')}
    ];
    ` :''}
    protected $fillable = [
        ${table.fillable.map(d=>`'${d}',`).join('\n        ')}
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
`

export default template