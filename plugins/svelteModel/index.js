// @ts-check
import path from 'path'
import { createFolderInCaseOfError } from '../../src/ensureFolderexists.js'
import modelTemplate from './templates/models.js'
import { writeFileSync } from 'fs'
import { table } from 'console'

let __dirname = path.resolve(path.dirname(''))

/** @typedef {import('../../src/BaseObjects').Table} Table */

/** @param {string} targetFolder  */
function excecute(targetFolder) {

    /** @param {Table[]} tables  */
    return (tables) => {

        tables.map( table => {
            const storesDir = path.join(__dirname, targetFolder)
            let modelForTable = modelTemplate(table)
            console.log(storesDir)
            createFolderInCaseOfError(storesDir, () => {
                writeFileSync(path.join(storesDir, table.name + '.js'), modelForTable)
            })
        })

        return tables
    }
}

export default excecute