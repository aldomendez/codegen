//@ts-check
/** @typedef {import('../../../src/BaseObjects').Table} Table */
/** @param {Table} table  */
let template = (table) => `
{
    name: "${table.label}",
    table: "${table.name}",
    fields: [
      ${table.fields.map(f => `{
        field: "${f.name}",
        label: "${f.label}",
        type: "string",
        help: "",
        length: ${f.length},
        size: 2
      },`).join('\n')}
    ],
    primary: "${table.primaryKey}",
    visible: [${table.fields.map( f => `'${f.name}',`).join('')}],
    disableDelete: false,
    editable: [${table.fields.map( f => `'${f.name}',`).join('')}],
    // formTabs: [
    //   {
    //     name: "Generals",
    //     description: "General company information",
    //     fields: ["name", "description", "active"]
    //   }
    // ],
    relations: {
    //   user: { by: "id" }
    },
    controllers: {
    //   api: {
    //     origin: "api/company",
    //     methods: ["index", "store", "update", "show", "destroy"]
    //   }
    }
  }
  `
// ${table.fields.map(d => `${d.setMethodName}: set('${d.name}'),`).join('\n    ')}
export default template