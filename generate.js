// @ts-check
import { allTables } from './databaseConfiguration.js'
import path from 'path'
import { readdir } from 'fs/promises'
import { readFileSync } from 'fs'
import { Command } from 'commander'
import keyBy from 'lodash/keyBy.js'

const program = new Command()

let __dirname = path.resolve(path.dirname(''))

readdir(path.join(__dirname, 'plugins')).then((files) => {
    // console.log(files)
    /**
     * First step is to get all available modules
     */
    return Promise.all(
        files//.filter(d => d.includes('.js'))
            .map((f) => {
                let name = f.replace('.js', '')
                return new Promise((resolve, reject) => {
                    import(path.join(__dirname, 'plugins', name, 'index.js'))
                        .then(m => resolve({ name, excecute: m.default }))
                        .catch(e => reject({ name, error: e }))
                })
            })
    )
})
    .then(m => keyBy(m, 'name'))
    .then(async (modules) => {
        // console.log(Object.keys(modules))
        /**
         * Second step is to get a list of all available projects available
         */
        //  readdir(path.join(__dirname, 'projects_src')).then((files)=>{})

        program
            .option('-p, --project <project>', 'Project file to expand')
            .parse(process.argv)

        const options = program.opts()

        if (options.project) {
            const project = await import(path.join(__dirname, 'projects_src', options.project + '.js'))
            //  console.log(project)
            const root = project.root


            project.tables.forEach(table => {
                Object.keys(table.plugins).map(p => {
                    modules[p].excecute({root, ...table.plugins[p],})(table)
                })
            });

            Object.keys(project.plugins).forEach(pg => {
                if (modules[pg] && modules[pg].excecute) {
                    modules[pg].excecute({ root, ...project.plugins[pg] })(project.tables)
                }
            });
        }
    }).catch(e => {
        console.error(e)
    })
